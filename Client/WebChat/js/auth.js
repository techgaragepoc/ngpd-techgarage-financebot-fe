    var RESTServerIP = "10.52.12.11";           // Logical name of the REST Server
    var RESTServerPort = "10086";          // Port of the REST Server
    var ContentType = "application/vnd.mstr.dataapi.v0+json"; // To be added in each request as a request header
        var IServer = "10.52.12.11";                  // Logical name of the MicroStrategy IServer
    var IServerPort = 34952;                      // Port of the MicroStrategy IServer
    var projectName = "APAC_CAD_Analytics(China)";   // Name of the MicroStrategy Project
    // var username = "administrator";               // Credentials to connect to the MicroStrategy Project
    // var password = "madmin";
    var BASE_URL = "http://" + RESTServerIP + ":" + RESTServerPort + "/json-data-api/";

//         var RESTServerIP = "10.52.12.11";           // Logical name of the REST Server
//     var RESTServerPort = "10086";          // Port of the REST Server
//     var ContentType = "application/vnd.mstr.dataapi.v0+json"; // To be added in each request as a request header
//         var IServer = "192.168.3.170";                  // Logical name of the MicroStrategy IServer
//     var IServerPort = 34952;                      // Port of the MicroStrategy IServer
//     var projectName = "Client Profitablity";   // Name of the MicroStrategy Project
//     // var username = "administrator";               // Credentials to connect to the MicroStrategy Project
//     // var password = "madmin";
//     var BASE_URL = "http://" + RESTServerIP + ":" + RESTServerPort + "/json-data-api/";



// //https://mercer.cloud.microstrategy.com:6443/MicroStrategy/servlet/mstrWeb 

function authenticateUser(username,password){
    var usr = (username.toLowerCase() == 'mariya' ? 'Pankaj':username);
    var result = null;
    console.log( BASE_URL + "sessions");
     $.ajax({
            type: 'POST',
            url: BASE_URL + "sessions",
            async:false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-IServerName", IServer);
                xhr.setRequestHeader("X-Port", IServerPort);
                xhr.setRequestHeader("X-ProjectName", projectName);
                xhr.setRequestHeader("X-Username", usr);
                xhr.setRequestHeader("Accept", ContentType);
                xhr.setRequestHeader("X-Password", password);
            },
            success: function (response) {
                result =  response;
            },
            error: function (e) {
                console.log(e);
                 result = e;
            }
        });
        return result;
}