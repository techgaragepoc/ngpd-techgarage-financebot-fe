'use strict'
$(".overlay").hide();
$('#start-time').html(postTime());
$('#greeting-line').html(postGreetingLine());
var isIdentified = false;
var userName = '';
var botCounter = 900;
var botTimer = null;
var tabCount = 1;


function updateCounter(){
botCounter--;
if(botCounter == 0){
        $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Logging out session due to in-activity.<br>Please <a href="index.html">sign in</a> again.'+botEnd);
        $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
        $("#query-box").attr("disabled", true);
        $("#query-button").attr("disabled", true);
        $('#indicator')
        .removeClass('text-success')
        .addClass('text-red'); 
         $("#btn-refresh").attr("disabled", true);  
clearInterval(botTimer);
}
else if(botCounter == 450){
    $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Do you have any other query?'+botEnd);
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
}
}
function startInterval()
{
botTimer= setInterval("updateCounter()", 1000);
}

$(document)
    .one('focus.textarea', '.form-control', function(){
        var savedValue = this.value;
        this.value = '';
        this.baseScrollHeight = this.scrollHeight;
        this.value = savedValue;
    })
    .on('input.textarea', '.form-control', function(){
        var minRows = this.getAttribute('data-min-rows')|0,
            rows;
        this.rows = minRows;
        rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 16);
        if(rows>0)
            this.rows = rows;
        else
            this.rows= 1;
        $('.direct-chat-messages').height(560-($('.box-footer').height()- 55));
    });
     $('#toolbar-button').on('click',function(){
        //$('.toolbox').toggle();
        $('.toolbox').fadeIn("slow", function() {
  });
     });

  $('#query-button').on('click',function(){
    $("textarea[id=query-box]").attr('rows','1'); 
        $('.direct-chat-messages').height(560-($('.box-footer').height()- 55));
        if(isIdentified){
            webChat();   
      }
        else{
            identifyUser();
      }
  });
    function initiateSurvey(){
        $('#notification').hide();
    }
    function submitFeedback(selector,obj){
        if(document.getElementById(selector).value ==''){
            alert('Please write something about your interaction.');
        }else{
        $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Thanks for your valuable feedback.'+botEnd);
        $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight); 
        $('#notification').show();
        $(obj).attr("disabled", "disabled");
        }
    }

    function submitAccessRequest(selector,obj){
        $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Thanks! Your request has been taken. We will get back to you to collect more information.'+botEnd);
        $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight); 
        $(obj).attr("disabled", "disabled");
    }

    function exportChat(){
        var lines = '<table style="border-collapse: collapse;">';
        chatHistory.forEach(function(item){
            var data = item.split(') -');
            if(data[0].indexOf('Chat-Bot')!= -1){
            lines+= '<tr><td style="min-width: 275px;background-color: #3c8dbc;color: white;vertical-align: top;">'+data[0]+') </td><td style="background-color: #3c8dbc;color: white;">'+data[1]+'</td></tr>';
            }
        else{
            lines+= '<tr><td style="min-width: 275px;vertical-align: top;">'+data[0]+') </td><td>'+data[1]+'</td></tr>';
        }
        });
        lines +='</table>';
        if ( window.navigator.msSaveOrOpenBlob && window.Blob ) {
    var blob = new Blob([lines], { type: "text/html;charset=utf-8" } );
    navigator.msSaveOrOpenBlob( blob, 'chat-'+userName+'.html' );
}else{
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/html;charset=utf-8,' + encodeURIComponent(lines));
  element.setAttribute('download', 'chat-'+userName+'.html');

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}
    }
function quickExport(quickSwitchCount){
       $(".overlay").show(); 
        var csvString = '';
$('#qs-table-'+quickSwitchCount+' tr').each(function(){
     $(this).find('th').each(function(){
        csvString += ($(this).text()+',');
    })
    $(this).find('td').each(function(){
        csvString += ($(this).text().replace(/,/g,'')+',');
    });
    csvString += '\n';
});
 if ( window.navigator.msSaveOrOpenBlob && window.Blob ) {
    var blob = new Blob([csvString], { type: "data:text/csv;charset=utf-8" } );
    navigator.msSaveOrOpenBlob( blob, 'datasheet-'+quickSwitchCount+'.csv' );
}else{
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvString));
  element.setAttribute('download', 'datasheet-'+quickSwitchCount+'.csv');
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

 $(".overlay").hide();
}
function exportToCSV(csv) {

 
var csvData = 'data:text/csv;charset=utf-8,' + csv;

//For IE

if (navigator.appName == "Microsoft Internet Explorer") {

myFrame.document.open("text/html", "replace");

myFrame.document.write(uuu);

myFrame.document.close();

myFrame.focus()

myFrame.document.execCommand('SaveAs', true, 'data.xls');

}

else {

//Others

$(this).attr({'href': csvData, 'target': '_blank' });

}

}
    function quickSwitch(quickSwitchCount){
        $(".overlay").show(); 
if($('#qs-icon-'+quickSwitchCount).hasClass('fa-area-chart')){
    $('#qs-icon-'+quickSwitchCount).removeClass('fa-area-chart');
    $('#qs-icon-'+quickSwitchCount).addClass('fa-table');
        var graphData = [];
$('#qs-table-'+quickSwitchCount+' tr').each(function(){
    var row = [];
     $(this).find('th').each(function(){
        //console.log($(this).text());
        row.push($(this).text());
    })
    var isText = true;
    $(this).find('td').each(function(){
        if(isText){
            isText=false;
        row.push($(this).text());
        }
    else{
        row.push(parseFloat($(this).text().replace(/,/g,'')));
    }
    });
    graphData.push(row);
    if(graphData.length>=5)
        graphData.length = 5;
})

   google.charts.load('current', {'packages':['bar']});
   google.charts.setOnLoadCallback(function() { drawChart(quickSwitchCount,graphData); });
}
else if($('#qs-icon-'+quickSwitchCount).hasClass('fa-table')){
    $('#qs-icon-'+quickSwitchCount).removeClass('fa-table');
 $('#qs-icon-'+quickSwitchCount).addClass('fa-area-chart');
    $('#qs-graph-'+quickSwitchCount).hide();
    $('#qs-table-'+quickSwitchCount).show();
    $(".overlay").hide(); 
    // $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
}
    }
function drawChart(quickSwitchCount,graphData) {
    $('#qs-graph-'+quickSwitchCount).show();
    $('#qs-table-'+quickSwitchCount).hide();
    
        var data = google.visualization.arrayToDataTable(graphData);
        console.log(data);
var containerWidth = $('#qs-container-'+quickSwitchCount).width();
//containerWidth -=18;
        var options = {
              responsive: true,
            axisTitlesPosition:'in',
                   hAxis: {
                      textStyle: {
                            color: 'black'
                            },
                   },
             vAxis :
                        {
                            gridlines: {
                            color: 'transparent'
                            },
                            textStyle: {
                            fontSize: '0'
                            },
                        },
    //                          vAxis: {
    //              title: '',
    //             //showTextEvery:1,
    //     direction:-1,
    //     slantedText:true,
    //     slantedTextAngle:45 // here you can even use 180
    // },
            legend:{position:'none'},
            // qs-container-4
               width:containerWidth,
        // height: 400,
        bar: {width: "50%"},
        //   chart: {
        //     title: 'Company Performance',
        //     subtitle: 'Sales, Expenses, and Profit: 2014-2017',
        //   },
          bars: 'vertical' // Required for Material Bar Charts.
        };

           var chart = new google.charts.Bar(document.getElementById('qs-graph-'+quickSwitchCount));


        chart.draw(data, google.charts.Bar.convertOptions(options));
        $(".overlay").hide(); 
        // $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
      }
    var isLast = false;
    $('#btn-refresh').on('click',function(){
        chatHistory.length = 0;
        $(".overlay").show();  
        // $(".direct-chat-messages").html(botStart+postTime()+botConcat+postGreetingLine()+', May I know your Name?'+botEnd);   
       $(".direct-chat-messages").html(botStart+postTime()+botConcat+postGreetingLine()+' '+userName+'! How can I help you?'+botEnd);
       
        //isIdentified= false;
        $(".overlay").hide();  
        $("#query-box").attr("disabled", false);
        $("#query-button").attr("disabled", false);
        $("#btn-refresh").attr("disabled", false);
        
        pushVideoMessage();                
        
    });
    function getValueUsingClass(className){
	/* declare an checkbox array */
	var chkArray = [];
	while(chkArray.length > 0) {
    chkArray.pop();
    }
	/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$("."+className+":checked").each(function() {
		chkArray.push($(this).val());
	});
	
	/* we join the array separated by the comma */
	var selected;
	selected = chkArray.join(',') ;
	
	// /* check if there is selected checkboxes, by default the length is 1 as it contains one single comma */
	// if(selected.length > 0){
	// 	alert("You have selected " + selected);	
	// }else{
	// 	alert("Please at least one of the checkbox");	
    // }
    return chkArray;
}
function disableSelection(className){
	/* declare an checkbox array */
	/* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
	$("."+className+":checked").each(function() {
		$(this).removeAttr('checked');
	});
}
 $('#query-box').on("focus", function(e) {
       $('.toolbox').fadeOut("slow", function() {
  });
 });
  $('#query-box').on("keydown", function(e) {
    
        if (e.keyCode == 13) {
            $("textarea[id=query-box]").attr('rows','1'); 
        $('.direct-chat-messages').height(560-($('.box-footer').height()- 55));
          if(isIdentified){
                webChat();   
                botCounter = 900;
            }
            else{
                identifyUser();
            }
            return false; 
        }
        else if(e.keyCode == 38){//up
            if(recordCounter > 0){
                recordCounter = parseInt(recordCounter) - 1;
                if((recordCounter >= 0) && (recordCounter<= recordSet.length))
                    $("#query-box").val('').val(recordSet[recordCounter]);
            }
        }
        else if(e.keyCode == 40){//down
            if(recordCounter < recordSet.length){
                recordCounter = parseInt(recordCounter) + 1;
                if((recordCounter >= 0) && (recordCounter<= recordSet.length))
                    $("#query-box").val('').val(recordSet[recordCounter]);
            }
        }
});

var recordSet = [];
var recordCounter = 0;
var msgConcat ='</span></div><img class="direct-chat-img" src="img/user3-128x128.jpg" style="height:38px !important;margin-top:2px !important;" alt="Message User Image"><div class="direct-chat-text">';
var msgEnd = '</div></div>';


var botStart = '<div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">Chat-Bot</span><span class="direct-chat-timestamp pull-right">';
var botConcat = '</span></div><img class="direct-chat-img" src="img/roboto-logo.png" alt="Message User Image"><div class="direct-chat-text">';
var botEnd = '</div> </div>';
var botName = 'Chat-Bot';
var chatHistory = [];
function getmsgStart(){
    return '<div class="direct-chat-msg right"> <div class="direct-chat-info clearfix"> <span class="direct-chat-name pull-right">'+userName+'</span><span class="direct-chat-timestamp pull-left">';
}
function identifyUser(){
     userName = getParameterByName('user');//toTitleCase($("#query-box").val());
     userName = toTitleCase((userName == null ? 'Pankaj Rautela':userName));
                 $(".overlay").show(); 
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+postGreetingLine()+' '+userName+'! How can I help you?'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+postGreetingLine()+'! How can I help you?');
                 $("#query-box").val('');
                 $(".overlay").hide();
                 $("#query-box").focus();
                isIdentified = true;

        pushVideoMessage();
}
function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}    
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
var requestCount = 0;
function raiseRequest(){
        var outline ='<span style="font-size: 12px;">**Above mentioned cubes are not configured to be queried by chatbot. Do you want to raise configuration request?</span>';
        outline+='<br><a id="req-yes-'+requestCount+'" href="javascript:void(0);" onclick=requestAccess(this,'+requestCount+') class="rounded-selector">Yes</a>';
        outline+='<a id="req-no-'+requestCount+'" href="javascript:void(0);" class="rounded-selector" onclick=askMore('+requestCount+')>No</a><br>';
        outline +='&nbsp;';
        requestCount++;
         $(".direct-chat-messages").append(botStart+postTime()+botConcat+outline+botEnd);
         chatHistory.push(botName+' ('+postTime()+') - '+outline);
        $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight); 
}
    var isFAQSelected = false;
function webChat(){
    if($("#query-box").val().length>0){
        var userQuery = $("#query-box").val();
        $(".overlay").show(); 
            //capturing query in record set.
            if(recordSet.indexOf(userQuery) == -1)
                recordSet.push(userQuery);

            recordCounter++;
            //end code
    $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+userQuery+msgEnd);
    chatHistory.push(userName+'('+postTime()+') - '+userQuery);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/chat",
            async:false,
            data: { query: userQuery, lang: "en", user: userName, dateTime: new Date(), isFAQ:isFAQSelected },
            success: function (htmlResponse) {
                if(htmlResponse){
                    if(htmlResponse.rate){
                        $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
                            $('#'+htmlResponse.rate).barrating({
                                theme: 'css-stars',
                                showSelectedRating: false,
                                initialRating:0
                            }); 
                    }
                    else if(htmlResponse.raiseRequest){
                          $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                        chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
                        raiseRequest();
                    }
                    else if(htmlResponse.crossTab){
                        
                        var divName = '#crosstab-'+tabCount;
                        var result = htmlResponse.data+"<br><br><div id='crosstab-"+(tabCount++)+"'></div>";
                        $(".direct-chat-messages").append(botStart+postTime()+botConcat+result+botEnd);
                        chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
                        $(divName).pivot(htmlResponse.crossTab, {
                        cols: [htmlResponse.column],
                        rows: [htmlResponse.row],
                        aggregator: $.pivotUtilities.aggregators["Integer Sum"]([htmlResponse.aggregate]),
                        vals: ["live_listings"],
                        rendererName: "Table"
                        });
                        if(htmlResponse.hideTotals){
                         hideTotals(divName);   
                        }
                    }
                    else if(htmlResponse.data.indexOf("I am not trained")>-1){
                        updateUtterances();
                          $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
                  if(htmlResponse.data.indexOf("fancy-box">0)){
                     $("a#fancy-box").fancybox({
                           'titlePosition'	: 'inside'
                      });
                  }
                    }
                    else{
                $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
                  if(htmlResponse.data.indexOf("fancy-box">0)){
                     $("a#fancy-box").fancybox({
                           'titlePosition'	: 'inside'
                      });
                  }
                    }
                   // startInterval();
                }
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
    }
    else{
         $("#query-box").focus();
    }

}
updateUtterances();
function updateUtterances(){
      var table = document.getElementById("utterance-tab");
            var tableRows = table.getElementsByTagName('tr');
            var rowCount = tableRows.length;

            for (var x=rowCount-1; x>0; x--) {
            table.removeChild(tableRows[x]);
            }
 $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/GetUnmappedUtterances",
            async:true,
            // data: { project: projectName},
            success: function (htmlResponse) {

                var array = ["","Intent Extension","Utterance Extension","Entity Extension","Typo Error","Out of scope"];

//Create and append select list


            var table = document.getElementById("utterance-tab");
            var tableRows = table.getElementsByTagName('tr');
            var rowCount = tableRows.length;

            for (var x=rowCount-1; x>0; x--) {
            table.removeChild(tableRows[x]);
            }
            //   console.log(htmlResponse.list);
              htmlResponse.data.forEach(function(item,index){
            var tr = document.createElement("tr");
            
            var td = document.createElement("td");
            td.align="center";
            var txt = document.createTextNode((++index)+".");
            td.appendChild(txt);
            tr.appendChild(td);
            table.appendChild(tr);


             td = document.createElement("td");
             txt = document.createTextNode(item._id);
             td.appendChild(txt);
            tr.appendChild(td);
            table.appendChild(tr);
            
               td = document.createElement("td");
             txt = document.createTextNode(item.count);
            td.align="center";
             td.appendChild(txt);
            tr.appendChild(td);
            table.appendChild(tr);

            var selectList = document.createElement("select");
selectList.id = "mySelect"+index;
//Create and append the options
for (var i = 0; i < array.length; i++) {
    var option = document.createElement("option");
    option.value = array[i];
    option.text = array[i];
    selectList.appendChild(option);
}
selectList.style.margin="5px";
              td = document.createElement("td");
             td.appendChild(selectList);
            tr.appendChild(td);
            table.appendChild(tr);

            

              });
            },
            error: function (err) {
                console.log(err);
                 
            }
        });
}
    function bindModel(bool){
        isFAQSelected = bool;
    }
function getReports(projectName){
        $(".overlay").show(); 
    $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+projectName+msgEnd);
    chatHistory.push(userName+'('+postTime()+') - '+projectName);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/ReportList",
            async:false,
            data: { project: projectName},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') -Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}
 $('.rounded-selector').click(function(){
     $(this).addClass('active');
 });
var countCarousel = 0;

var navControls = function() {

    if($('#botCarousel'+countCarousel).find("div.item").first().hasClass("active")){

        $('#botCarousel'+countCarousel).find('.left.carousel-control').hide();
        
        if($('#botCarousel'+countCarousel).find("div.item").length>1){
            $('#botCarousel'+countCarousel).find('.right.carousel-control').show();
        }
        else{
            $('#botCarousel'+countCarousel).find('.right.carousel-control').hide();
        }
    } else if($('#botCarousel'+countCarousel).find("div.item").last().hasClass("active")){
 
       $('#botCarousel'+countCarousel).find('.right.carousel-control').hide();
       $('#botCarousel'+countCarousel).find('.left.carousel-control').show();
    } else {

        $('#botCarousel'+countCarousel).find('.right.carousel-control').show();
       $('#botCarousel'+countCarousel).find('.left.carousel-control').show();
    }
}
var accessCount = 0;

function requestAccess(obj,index){
        $(obj).attr("disabled", "disabled");
   var outline = 'Please help us to understand your requirement - </br><table class="access-table" style="width: 100%;"><tr><td style="width:45%;">Requester Email ID:</td><td> <input type="text" class="textblock" value="pankaj.rautela@mercer.com" disabled="disabled"></td></tr><tr><td>Metrics Name:</td><td> <input type="text" class="textblock"></td></tr><tr><td>Associated Dimensions:</td><td> <input type="text" class="textblock"></td></tr><tr><td valign="top">Sample Queries:</td><td> <textarea placeholder="" class="rating-control" data-min-rows="2" type="text" rows="2"></textarea></td></tr><tr><td><input class="btn btn-primary btn-flat" onclick=submitAccessRequest("rating-'+accessCount +'",this) value="Submit" type="button"></td><td></td></tr></table>';

    $(".direct-chat-messages").append(botStart+postTime()+botConcat+outline+botEnd);
        $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight); 
   accessCount++;
   document.getElementById('req-yes-'+index).removeAttribute("onclick");
   document.getElementById('req-no-'+index).removeAttribute("onclick");
}
function askMore(index){
     $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Anything else I can help you with...'+botEnd);
        $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight); 
    document.getElementById('req-yes-'+index).removeAttribute("onclick");
    document.getElementById('req-no-'+index).removeAttribute("onclick");
}

function getFAQ(category,carouselContainer,aTag){
    $(aTag).parent().find('a').each(function(e) {
        $(this).removeClass("active");
});
    $(aTag).addClass('active');
      $(".overlay").show(); 
    //   $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+category+msgEnd);
    $("#query-box").val('');
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/FAQ",
            async:false,
            data: { faqCategory: category},
            success: function (htmlResponse) {
var outline = '<div id="botCarousel'+(++countCarousel)+'" class="carousel slide" style="background-color: white;padding: 5px;margin: 5px;border-radius: 5px;" data-ride="carousel" data-interval="false" data-wrap="false"><ol class="carousel-indicators"><li data-target="#botCarousel" data-slide-to="0" class="active"></li><li data-target="#botCarousel" data-slide-to="1"></li><li data-target="#botCarousel" data-slide-to="2"></li></ol><div class="carousel-inner" style="min-height:250px">';
var isFirst = true;
  htmlResponse.data.forEach(function(item){
                    if(isFirst){
                                    outline += '<div class="item active">';
                                    isFirst=false;
                    }
                    else{
                        outline += '<div class="item">';
                    }
                    outline += '<span style="color:#3c8dbc;">Q:'+item._id.Question+'</span><br/>';
                    outline += '<span style="color:black;">A:'+item._id.Answer+'</span><br/>';
                    outline += '</div>';
                });
outline += '<a class="left carousel-control" title="Previous" href="#botCarousel'+countCarousel+'" data-slide="prev" style="color:#3c8dbc;background-image:none;width:8%"><span class="glyphicon glyphicon-chevron-left"></span><span class="sr-only">Previous</span></a><a class="right carousel-control" href="#botCarousel'+countCarousel+'" data-slide="next" style="color:#3c8dbc;background-image:none;width:8%" title="Next"><span class="glyphicon glyphicon-chevron-right"></span><span class="sr-only">Next</span></a></div>';
$('#qna'+carouselContainer).html(outline);

navControls();

            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}

function getClaimAnalysis(analysisType){
        $(".overlay").show(); 
    $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+analysisType+msgEnd);
    chatHistory.push(userName+'('+postTime()+') - '+analysisType);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/ClaimAnalysis",
            async:false,
            data: { claimType: analysisType},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}
    function pushVideoMessage(){
      $(".overlay").show(); 
    //   var videoString = '<video height="240" controls><source src="https://www.w3schools.com/html/movie.mp4" type="video/mp4"></video>';
    //  $(".direct-chat-messages").append(botStart+postTime()+botConcat+videoString+botEnd);
         $(".overlay").hide(); 
    }
    function refreshDashboardData(cubeId,collectionName,dashboardName){
     $(".overlay").show(); 
     $(".direct-chat-messages").append(botStart+postTime()+botConcat+"Please wait while i am refreshing "+'<bld>'+decodeURIComponent(dashboardName)+"</bld>"+botEnd);
    //$(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+dashboardName+msgEnd);
    chatHistory.push(userName+'('+postTime()+') - '+decodeURIComponent(dashboardName));

    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/RefreshDataSet",
            async:false,
            data: { CubeId: cubeId, CollectionName: collectionName},
            success: function (htmlResponse) {
                
                if(htmlResponse.status == 'complete'){
                $(".direct-chat-messages").append(botStart+postTime()+botConcat+'<bld>'+decodeURIComponent(dashboardName)+"</bld> has been refreshed"+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+'<bld>'+decodeURIComponent(dashboardName)+"</bld> has been refreshed");
                }
                else{
                $(".direct-chat-messages").append(botStart+postTime()+botConcat+'<bld>'+decodeURIComponent(dashboardName)+'</bld> cannot be refreshed.'+botEnd);
                }
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}
function hideTotals(selectorName){
    $(selectorName).find('.pvtGrandTotal').hide();
    $(selectorName).find('.pvtTotal').hide();
    $(selectorName).find('.pvtTotalLabel').hide();
}
function getReportsByMetric(APACBIMetricsEntity){
    $(".overlay").show(); 
    $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+'Yes'+msgEnd);
    chatHistory.push(userName+':'+postTime()+'- Yes');
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/ReportsByMetric",
            async:false,
            data: { MetricName: APACBIMetricsEntity},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}

function getTalentReports(projectName){
    $(".overlay").show(); 
    $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+projectName+msgEnd);
    chatHistory.push(userName+'('+postTime()+') - '+projectName);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/TalentReports",
            async:false,
            data: { ProjectName: projectName},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}

function ExecuteReport(reportName){
      $(".overlay").show(); 
    $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+reportName+msgEnd);
    chatHistory.push(userName+'('+postTime()+') - '+reportName);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/ExecuteReport",
            async:false,
            data: { ReportName: reportName},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
                 if(htmlResponse.data.indexOf("fancy-box">0)){
                     $("a#fancy-box").fancybox({
                           'titlePosition'	: 'inside', 
//                             selector : '[data-fancybox="images"]',
//   loop     : true
                      });
                  }
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}

function getCubeInformation(cubeName){
    $(".overlay").show(); 
    $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+cubeName+msgEnd);
    chatHistory.push(userName+'('+postTime()+') - '+cubeName);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/CubeInformation",
            async:false,
            data: { CubeName: cubeName},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
                 if(htmlResponse.data.indexOf("fancy-box">0)){
                     $("a#fancy-box").fancybox({
                           'titlePosition'	: 'inside'
                      });
                  }
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}
function getPremiumAnalysis(analysisType){
        $(".overlay").show(); 
    $(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+analysisType+msgEnd);
    chatHistory.push(userName+'('+postTime()+') - '+analysisType);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/PermiumAnalysis",
            async:false,
            data: { premiumType: analysisType},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
}
function selectPeriodPrompt(){
           $(".overlay").show(); 
    //$(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+projectName+msgEnd);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/PromptAnalysis",
            async:false,
            data: { promptType: 3, promptName:'Period'},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
    }

    function runReport(report){
        window.open(report,'_blank'); 
}
    var periodArray = [];
    var BusinessArray = [];
    var RegionArray = [];
    function setPeriodArray(className){
        while(periodArray.length > 0) {
    periodArray.pop();
    }
     periodArray.push(className);// = getValueUsingClass(className);
     //disableSelection(className);
     console.log('periods - '+periodArray);
    }

        function setBusinessArray(className){
                while(BusinessArray.length > 0) {
    BusinessArray.pop();
    }
     BusinessArray = getValueUsingClass(className);
     //disableSelection(className);
     console.log('Business - '+BusinessArray);
    }

        function setRegionArray(className){
             while(RegionArray.length > 0) {
    RegionArray.pop();
    }
     RegionArray = getValueUsingClass(className);
    // disableSelection(className);
     console.log('Region - '+RegionArray);
    }
    
    function selectBusinessPrompt(previousPromptClass){
        //document.getElementById("region-prompt").disabled = true;
        $("input#region-prompt").attr("disabled", true);
       
    setRegionArray(previousPromptClass);
           $(".overlay").show(); 
    //$(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+projectName+msgEnd);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/PromptAnalysis",
            async:false,
            data: { promptType: 1, promptName:'Business'},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                  chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
    }

    
function generateDROAnalysis(metricName, region, promptClass){
    //document.getElementById("business-prompt").disabled = true; 
    $("input#dro-period-prompt-selector").attr("disabled", true);
    $(".overlay").show(); 
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/GenerateDROAnalysis",
            async:false,
            data: { period: $("#"+promptClass).val(), metric:metricName, region:region},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
    }

function generateAnalysis(previousPromptClass){
    //document.getElementById("business-prompt").disabled = true; 
    $("input#business-prompt").attr("disabled", true);
    setBusinessArray(previousPromptClass);
           $(".overlay").show(); 
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/GenerateAnalysis",
            async:false,
            data: { period: periodArray, business:BusinessArray, region:RegionArray},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
    }

    function runReport(report){
        window.open(report,'_blank'); 
}




function selectRegionPrompt(previousPromptClass){
    //document.getElementById("period-prompt").disabled = true; 
    $("input#period-prompt").attr("disabled", true);
    setPeriodArray($("#"+previousPromptClass).val());
           $(".overlay").show(); 
    //$(".direct-chat-messages").append(getmsgStart()+postTime()+msgConcat+projectName+msgEnd);
    $("#query-box").val('');
    $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $.ajax({
            type: "POST",
            url: "http://10.197.132.81:3978/api/PromptAnalysis",
            async:false,
            data: { promptType: 2, promptName:'Region'},
            success: function (htmlResponse) {
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+htmlResponse.data+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - '+htmlResponse.data);
            },
            error: function (err) {
                console.log(err);
                 $(".direct-chat-messages").append(botStart+postTime()+botConcat+'Oops, I am not able to connect with Bot Server. Please check your connectivity.'+botEnd);
                 chatHistory.push(botName+' ('+postTime()+') - Oops, I am not able to connect with Bot Server. Please check your connectivity.');
            }
        });
         $(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);  
     $(".overlay").hide();
     $("#query-box").focus();
    }

    function runReport(report){
        window.open(report,'_blank'); 
}
function postTime(){
var date = new Date();
return date.toDateString() +' '+ date.toLocaleTimeString();
}

function postGreetingLine(){
    var today = new Date()
var curHr = today.getHours()
    if (curHr < 12) {
        return 'Good Morning'
    } else if (curHr < 18) {
        return 'Good Afternoon'
    } else {
        return 'Good Evening'
    }
}