// This loads the environment variables from the .env file
require('dotenv-extended').load();

var restify = require('restify');
var Store = require('./store');
var JsonAPI = require('./JsonAPI');
const parse = require('./_parse');
var path = require('path');
const downloadFile = "./IoT.csv";
const uploadFile = "./utterances.json";

var utterances= [];

// Setup Restify Server
var server = restify.createServer();


var configParse = {
    inFile: path.join(__dirname, downloadFile),
    outFile: path.join(__dirname, uploadFile)
};

parse(configParse)
    .then((model) => {
        // Save intent and entity names from parse
        utterances = model.utterances;
        return;
    });

      
//Getting data from Cube using JSON DATA API
// JsonAPI.getSession().then(function(session){
// JsonAPI.getCubeData(session.authToken)
//     .then(function(result){
//       Store.deleteCubeData('dro')
//                 .then(function () {
//                     Store.insertCubeData(result,'dro')
//                     .then(function () {
                            // Setup Restify Server
                            server.listen(process.env.port || process.env.PORT || 3978, function () {
                                console.log('%s listening to %s', server.name, server.url);
                            });

                            server.use(restify.bodyParser());
                            server.use(function crossOrigin(req,res,next){
                                res.header("Access-Control-Allow-Origin", "*");
                                res.header("Access-Control-Allow-Headers", "X-Requested-With");
                                return next();
                            });   
//                     });     
//                 });
//     });
// });
var webChat = function(req, res, next) {
    if(req.params.isFAQ == 'false'){
        Store.getIntent(req.params.query)
                .then(function (result) {
                    var queryData = {
                        query:req.params.query,
                        dateTime:  req.params.dateTime,
                        userName: req.params.user
                    };
                    // var response = {
                    //     userName: req.params.user,
                    //     dateTime: req.params.dateTime,
                    //     luis: result
                    // };
                //     Store.saveLuisResponse(response)
                // .then(function () {
                    switch(result.topScoringIntent.intent){
                        case 'Information':
                         var cubeEntity =result.entities[result.entities.findIndex(function(val) {
                                      return val.type == 'Cube'
                                    })];
                        var aPACBIMetricsEntity =result.entities[result.entities.findIndex(function(val) {
                          return val.type == 'APACBIMetrics'
                        })];
                        var financeMetricsEntity =result.entities[result.entities.findIndex(function(val) {
                          return val.type == 'FinanceMetrics'
                        })];
                       var chatBotEntity =result.entities[result.entities.findIndex(function(val) {
                          return val.type == 'ChatBot'
                        })];

                        if(cubeEntity){
                            Store.SqlDataResponse("SELECT DISTINCT REP_NAME,DOC_GUID,REP_DESC FROM VW_DATALAB WHERE REP_NAME IN ('AR & WIP Analysis Cube','DRO Analysis Cube')")
                                    .then(function (dataset) {
                            var outline = 'I have scanned APAC + US BI project\'s repositories and found following cubes that can be directly consumed -<br/>';
                            
                                        dataset.forEach(function (item,index){
                                            outline += '<br/>'+(index+1)+'. <bld>'+item.REP_NAME+'</bld> - '+item.REP_DESC+'. <bld> <a target="_blank" href="http://10.52.12.11/MicroStrategy/asp/Main.aspx?evt=3140&src=Main.aspx.3140&documentID='+item.DOC_GUID+'&Server=AUMEL13AS26V&Project=APAC_DATALAB&Port=0&share=1&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,footer">Generate Analytics</a></bld><br/>';
                                     //outline += '<br/>'+(index+1)+'. <a target="_blank" href="http://aumel13as26v/MicroStrategy/asp/Main.aspx?evt=3140&src=Main.aspx.3140&documentID='+item.DOC_GUID+'&Server=AUMEL13AS26V&Project=APAC_DATALAB&Port=0&share=1&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,footer">'+item.REP_NAME+'</a>';
                                     });
                                     outline +='<br><span style="text-decoration: underline;text-decoration-style: dashed;font-size: 12px;">Above configurations are part of business cases received from finance team to understand solution capablities.</span>'
                                    outline +='<br/>&nbsp;';
                            res.json({
                                        data: outline
                                    });  
                                    });
                        }
                        else if(aPACBIMetricsEntity){
  Store.SqlDataResponse("select distinct rep_name,DOC_GUID,REP_DESC from VW_DataLab where MET_NAME like '%"+aPACBIMetricsEntity.resolution.values[0]+"%' and REPORT_TYPE='Intelligent Cube' and doc_name like 'VI%'")
                                    .then(function (dataset) {
                            var outline = 'I have found following data cubes in APAC CAD CHINA Analytics project  which has '+aPACBIMetricsEntity.resolution.values[0]+' metric in it -<br/>';
                            
                              dataset.forEach(function (item,index){
                                            outline += '<br/>'+(index+1)+'. <bld>'+item.rep_name+'</bld> - '+item.REP_DESC+'. <bld> <a target="_blank" href="http://10.52.12.11/MicroStrategy/asp/Main.aspx?evt=3140&src=Main.aspx.3140&documentID='+item.DOC_GUID+'&Server=AUMEL13AS26V&Project=APAC_DATALAB&Port=0&share=1&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,footer">Generate Analytics</a></bld><br/>';
                                     //outline += '<br/>'+(index+1)+'. <a target="_blank" href="http://aumel13as26v/MicroStrategy/asp/Main.aspx?evt=3140&src=Main.aspx.3140&documentID='+item.DOC_GUID+'&Server=AUMEL13AS26V&Project=APAC_DATALAB&Port=0&share=1&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,footer">'+item.REP_NAME+'</a>';
                                     });

                                    //     dataset.forEach(function (item,index){
                                    //  outline += '<br/>'+(index+1)+'. <a target="_blank" href="http://aumel13as26v/MicroStrategy/asp/Main.aspx?evt=3140&src=Main.aspx.3140&documentID='+item.DOC_GUID+'&Server=AUMEL13AS26V&Project=APAC_DATALAB&Port=0&share=1&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,footer">'+item.rep_name+'</a>';
                                    //  });
                              
                                     outline+='&nbsp;';
                            res.json({
                                        data: outline,
                                        raiseRequest:true
                                    });  
                                    });
                        }
                        else if(financeMetricsEntity){

                                    res.json({
                                        data: 'financeMetricsEntity'
                                    });  
                        }
                        else if(chatBotEntity){
                            res.json({
                                        data: '<video height="240" controls><source src="zoom_0.mp4" type="video/mp4"></video>'
                                    });  
                        }
                        else{
                             Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });
                        }
                
                        break;
                        case 'Greeting':
                         res.json({
                           
                        data: 'I can help you analyze data using <grn><bld>Narrative Science</bld></grn> (which combines <bld>AI</bld> and <bld>NLP</bld>): <br/><ul type="number"><li>Execute existing reports in Microstrategy, either through guided interaction or with specific parameters</li><li>Generate analytics from various data cubes</li><li>Generate additional insights from existing reports</li></ul>'
                    });   
                        break;
                        case 'thanks':
                        var outline = 'How helpful this interaction was ?</br><textarea id="rating-feedback-'+ratingCount+'" name="rating-feedback" placeholder="" class="rating-control" data-min-rows="2" type="text" rows="2"></textarea><div class="stars stars-example-css" style="padding-top: 5px;padding-bottom: 9px;"><select id="rating-'+ratingCount+'" name="rating" autocomplete="off"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option></select></div><input class="btn btn-primary btn-flat" onclick=submitFeedback("rating-feedback-'+ratingCount+'",this) value="Submit" type="button">';

                         res.json({
                        data:outline,
                        rate:'rating-'+(ratingCount++)
                    });   
                        break;
                        case 'Help': 
                          Store.FAQCategoryResponse()
                                                .then(function (categories) {
                                                    var outline ='Please select any category for <a alt="Revenue Growth: Connecting the Dots" title="Revenue Growth: Connecting the Dots" href="http://communities.mercer.com/revenuegrowth-connectingthedots/Library/Frequently%20Asked%20Questions.aspx" target="_blank" style="text-decoration: underline;text-decoration-style: dashed;">FAQ</a> - <br><br><table class="table table-bordered" style="background-color:white;"><tbody><div style="float:left">';
                          categories.forEach(function(item){
                            outline+='<a href="javascript:void(0);" onclick=getFAQ("'+encodeURIComponent(item)+'","'+carouselContainer+'",this) class="rounded-selector">'+item+'</a>';
                          });
                        
                         outline+='</div><div id="qna'+(carouselContainer++)+'" style="float:left;margin-top:10px;"></div>';
                          res.json({
                        data:outline
                    });   
                });
                        break;
                        case 'FinanceReporting':
                      
                        break;
                    case 'RefreshReportInfo':

                    var RefreshReportEntity = result.entities[result.entities.findIndex(function(val) {
                                                                  return val.type == 'RefreshReport'
                                                                })];
                    var FinanceReportsEntity = result.entities[result.entities.findIndex(function(val) {
                                                              return val.type == 'FinanceReports'
                                                            })];
                                                        if(RefreshReportEntity){
                    if(RefreshReportEntity.resolution.values[0] == 'Last report refresh'){

                           Store.GetMetaDataResponse(FinanceReportsEntity.resolution.values[0])
                                                .then(function (cubes) {
                        var outline = '<bld>'+FinanceReportsEntity.resolution.values[0]+'</bld> is a monthly dashboard. It was refreshed on '+cubes[0]._id.lastRefreshed+'<br/>';
                        outline +='<span style="font-size:13px;">If you wish to perform adhoc refresh please click <a alt="Refresh Dashboard"  onclick=refreshDashboardData("'+cubes[0]._id.cubeId+'","'+cubes[0]._id.collectionName+'","'+encodeURIComponent(FinanceReportsEntity.resolution.values[0])+'") title="Refresh Dashboard" href="javascript:void(0);" style="text-decoration: underline;text-decoration-style: dashed;">Here</a></span>';
                        // output += '<span style="font-size:14px;text-decoration-style: dotted;"> Do you want to refresh now?</span><br><br><table class="table table-bordered" style="background-color:white;"><tbody><div style="float:left"></div>';
                        
                        // output += '<a href="javascript:void(0);" onclick="refreshData(this)" class="rounded-selector">Yes</a>';
                        // output += '<a href="javascript:void(0);" class="rounded-selector">No</a>';
                        res.json({
                            data:outline
                                });    
                                                });
                    }
                    else if(RefreshReportEntity.resolution.values[0] == 'Next report refresh'){
                       res.json({
                            data:'<bld>'+FinanceReportsEntity.resolution.values[0]+'</bld> is a monthly dashboard. It will refresh on 1 April, 2018.'
                                });    
                    }
                    else if(RefreshReportEntity.resolution.values[0] == 'Report refresh frequency'){
            res.json({
                data:'Report refresh frequency for <bld>'+FinanceReportsEntity.resolution.values[0]+'</bld> is monthly.'
                    });    
                    }
                                                        }
                  else
                    {               
                      Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });   
                    }   

                    break;
                    case 'DataMining':
                    var dataMiningEntity =result.entities[result.entities.findIndex(function(val) {
                                                          return val.type == 'DecisionTree'
                                                        })];
                    if(dataMiningEntity){
                        if(dataMiningEntity.entity == 'churn'){
                            var outline ='Based on the latest data submitted to data-mining team, here is the summarized verdict towards <bld>"Customer Churn Prediction"</bld>:<br/><ul type="disc"><li><bld>Prediction Model</bld></li><ul type="circle"><li>Decision Tree Classification Model</li></ul><li><bld>Narrative Verdict</bld></li><ul type="circle"><li>We have processed historical record of overall 10000 telecom customers out of which 7500 are still active.</li><li>We analyized customer churn based on following categorical values: Customer, Gender, Marital Status, Income Bracket, Household count, Customer engagement factors like (start date, expiry date, active months, dropped calls, Net Present Value etc.</li><li>Decision processing model finalized <bld><rd>13</rd></bld> customers which accounts to <bld><rd>10.4%</rd></bld> churn propensity.</li></ul><li><bld>Action</bld></li><ul type="circle"><li>Please take proactive actions to mitigate churn risk. Please click on below link to access the list of leads.</li></ul></ul>';
                            outline +="<a id='fancy-box'  style='position: absolute;right: 60px;' href='./img/churn.png' title='This image depicts predictions across potential customer churn from Telecom industry.'><img alt='Decision Tree Prediction' src='' style='' title=''/></a>&nbsp;";
                             outline+= '&nbsp;<a style="position: absolute;right: 2px;bottom: 2px;" href="javascript:void(0);" onclick=runReport("http://10.197.135.69/MicroStrategy/asp/Main.aspx?evt=3140&src=Main.aspx.3140&documentID=71BD17A845A5A1FFFABDE78E4FC7C131&Server=INGGNED8N3YQG2&Project=MicroStrategy%20Tutorial&Port=0&share=1&uid=administrator&pwd=ladmin&hiddensections=header,path,dockTop,dockLeft,footer")>++more</a>';
                            
                        res.json({
                                data:outline
                            });  
                        }
                        else
                        {               
                           Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });   
                        }       
                    }
                    else
                    {               
                     Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });   
                    }                          
                    break;
                    case 'SurveyAnalysis':
                     var talentEntity =result.entities[result.entities.findIndex(function(val) {
                                      return val.type == 'TalentMetrics'
                                    })];
                     if(talentEntity){
                        if(talentEntity.entity == 'tier' || talentEntity.entity == 'tiers' || talentEntity.entity == 'level'){
                        res.json({
                                data:"<a id='fancy-box' href='./img/Tiers.jpg' title='This image depicts Tier wise span analysis for Mercer GTI and PM vs IC ratio for September, 2017.'><img alt='example5' src='./img/Tiers.jpg' style='width:190px;height:100px;' title='Click here to see large view'/></a>"    
                            });  
                        }
                        else if(talentEntity.entity == 'gender diversity' || talentEntity.entity == 'gender diversity ratio' || talentEntity.entity == 'male to female ratio' || talentEntity.entity == 'M:F'){
                            res.json({
                                data:"<a id='fancy-box' href='./img/gender.jpg' title='This image depicts gender diversity ratio across GTI by level 7 for September, 2017.'><img alt='example5' src='./img/gender.jpg' style='width:185px;height:100px;'  title='Click here to see large view'/></a>"
                            });  
                        }
                        else if(talentEntity.entity == 'headcount' || talentEntity.entity == 'employee headcount' || talentEntity.entity == 'employees headcount'){
                        res.json({
                                data:"<a id='fancy-box' href='./img/headcount.png' title='This image depicts Employee headcount for Mercer GTI by LOB for September, 2017.'><img alt='example5' src='./img/headcount.png' style='width:200px;height:100px;' title='Click here to see large view'/></a>"
                            });  
                        }
                        else
                        {               
                       Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });  
                        } 
                    }
                    else
                    {               
                    Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });
                    }         
                    break;
                        case 'BrowseProjects':

                     var domainEntity =result.entities[result.entities.findIndex(function(val) {
                                                                                      return val.type == 'Domain'
                                                                                    })];    
                            if(domainEntity){
                                    if(domainEntity.entity == 'finance'){  
                                        var projectList=['Client_Profitablity','People_Productivity'];
                                          var outline ='I have found following projects in "Enterprise Reporting" repository, you may choose any one from below list -';
                projectList.forEach(function(val,index){
                    outline += '<br/>'+(index+1)+'. <a href="javascript:void(0);" onclick=getReports("'+val+'")>'+val+'</a>';
                });
                                    res.json({
                                         data:outline
                                        });   
                                    }
                                    else  if(domainEntity.entity == 'health' || domainEntity.entity == 'h & b'){ 
                                           Store.searchProjects()
                                                .then(function (projects) {
                                                var outline ='I have found following projects in "APAC" repository, you may choose any one from below list -';
                                                projects.forEach(function(val,index){
                                                    outline += '<br/>'+(index+1)+'. <a href="javascript:void(0);" onclick=getReports("'+val+'")>'+val+'</a>';
                                                });
                                                res.json({
                                                            data:outline
                                                        });   
                                                });
                                    }
                                             else  if(domainEntity.entity == 'talent'){ 
                                                  var projectList=['Human_Resources','Learning&Development'];
                                                  var outline ='I have found following projects in "Talent" respository, you may choose any one from below list -';
                                                projectList.forEach(function(val,index){
                                                    outline += '<br/>'+(index+1)+'. <a href="javascript:void(0);" onclick=getTalentReports("'+val+'")>'+val+'</a>';
                                                });
                                                res.json({
                                                            data:outline
                                                        });   
                                    }
                                                     else{
                                  Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });  
                                }
                            }
                                else{
                                  Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });
                                }
                    break;
                          case 'BrowseReports':
                           var Aggregation =result.entities[result.entities.findIndex(function(val) {
                                      return val.type == 'countReports'
                                    })];
                            var APACBIMetricsEntity  =result.entities[result.entities.findIndex(function(val) {
                              return val.type == 'APACBIMetrics'
                            })];

                            var cubeEntity  =result.entities[result.entities.findIndex(function(val) {
                              return val.type == 'Cube'
                            })];

                         var dimensionsEntity =result.entities[result.entities.findIndex(function(val) {
                                      return val.type == 'Dimensions'
                                    })];
                            if(APACBIMetricsEntity){
                                if(Aggregation && cubeEntity){
                                       Store.CubeCountResponse(APACBIMetricsEntity.entity)
                                            .then(function (dataset) {
                                     var outline ='Presently, there are '+dataset[0].Value+' intelligent cube[s] with name <bld>'+dataset[0].Header+'</bld> that have <bld> '+toTitleCase(APACBIMetricsEntity.entity)+'</bld> metrics.' 
                                     if(dataset[0].Value>0){
                                            outline+= "<br/>Do you want to do analysis across it?";
                                            outline += '<br/><ul><li><a href="javascript:void(0);" onclick=getCubeInformation("'+dataset[0].Header+'")>Yes</a></li>';
                                            outline += '<li><a href="javascript:void(0);">No</a></li></ul>';
                                     }
                                                res.json({
                                                        data:outline
                                                    }); 
                                            });     
                                }
                                else if(Aggregation){
                                             Store.ReportCountResponse(APACBIMetricsEntity.entity)
                                            .then(function (dataset) {
                                            var outline ='Presently, there are '+dataset[0].Value+' reports that have <bld>'+  toTitleCase(APACBIMetricsEntity.entity)+'</bld> metrics.' 
                                            outline+= "<br/>Do you want to see list of All reports?";
                                            outline += '<br/><ul><li><a href="javascript:void(0);" onclick=getReportsByMetric("'+APACBIMetricsEntity.entity+'")>Yes</a></li>';
                                            outline += '<li><a href="javascript:void(0);">No</a></li></ul>';
                                                res.json({
                                            data:outline
                                        });    
                                    });
                                }
                                else
                                {
                                    
                                 Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });   
                                }
                            }
                            else
                            {
                                
                              Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });
                            }
                        break;
                          case 'HealthDataAnalysis':
                          var regionsEntity = result.entities[result.entities.findIndex(function(val) {
                                      return val.type == 'FinanceRegions'
                                    })];

                            var periodEntity = result.entities[result.entities.findIndex(function(val) {
                              return val.type == 'Period'
                            })];
                                
                                    var busniessEntity = result.entities[result.entities.findIndex(function(val) {
                              return val.type == 'Business'
                            })];
                        var dimensionEntity = result.entities[result.entities.findIndex(function(val) {
                              return val.type == 'Dimensions'
                            })]; 

                            var filterEntity =result.entities[result.entities.findIndex(function(val) {
                              return val.type == 'filter'
                            })];
                            var metricsEntity =result.entities[result.entities.findIndex(function(val) {
                              return val.type == 'FinanceMetrics'
                            })];
                        if(metricsEntity){
                            var metricName = 'metric';
                            var metricShortName = 'shortname';
                            if(metricsEntity.entity =='accounts receivable' || metricsEntity.entity.toLowerCase() =='ar' || metricsEntity.entity == 'receivable' || metricsEntity.entity == 'ar outstanding'){
                                metricName =  '$AR Outstandingv';
                                metricShortName = 'AR Outstanding';
                            }
                            else if(metricsEntity.entity.toLowerCase() =='unbilled wip' ||  metricsEntity.entity.toLowerCase() =='uwip'){
                                metricName = '$Unbilled Wipv';
                                metricShortName = 'Unbilled Wip';
                            } 
                            else if(metricsEntity.entity.toLowerCase() =='dro' ||  metricsEntity.entity.toLowerCase() =='days receivables outstanding' ||
                            metricsEntity.entity.toLowerCase() == 'days sales outstanding' ||
                        metricsEntity.entity.toLowerCase() == 'days receivable outstanding'){
                                //metricName = ""'$AR DROv','$WIP DROv'";
                                metricShortName = 'DRO';
                            } 
                            else if(metricsEntity.entity.toLowerCase() =='dror12' ||  metricsEntity.entity.toLowerCase() =='dro rolling 12' ||
                            metricsEntity.entity.toLowerCase() == 'dro r 12' ||
                        metricsEntity.entity.toLowerCase() == 'dro r12'){
                                //metricName = ""'$AR DROv','$WIP DROv'";
                                metricShortName = 'DRO Rolling 12';
                            } 

                            if(metricShortName == 'DRO' || metricShortName == 'DRO Rolling 12'){
                                if(regionsEntity && periodEntity){
                                        Store.DROResponse(regionsEntity.resolution.values[0],periodEntity.resolution.values[0],metricShortName)
                                        .then(function (financeData) {
                                                     Store.DROResponse(regionsEntity.resolution.values[0],getPreviousMonth(periodEntity.resolution.values[0]),metricName)
                                        .then(function (pmfinanceData) {
                                                Store.DROResponse(regionsEntity.resolution.values[0],getPreviousYear(periodEntity.resolution.values[0]),metricName)
                                         .then(function (pyfinanceData) {
                                            //  console.log(financeData[0].total);
                                            //  console.log(pmfinanceData[0].total);
                                            //  console.log(pyfinanceData[0].total);
                                                                           var outline = 'This analysis measures <bld>'+metricShortName+' Trend | As of '+periodEntity.resolution.values[0]+' for region '+regionsEntity.resolution.values[0]+'</bld>.<br>As per my analysis - <ul type="disc">';
//  - DRO value for pervious month was X which is less than/ greater than/ equal to this month value.
//  - DRO value for last year was X which is less than/ greater than/ equal to this month value.
// In summary DRO is moving up/ down across region/ the board since last month, but/ which is still higher/ lower than last year.
                                                                        if(financeData.length > 0){
                                                                            financeData.forEach(function(item){
                                                                                outline +='<li> '+metricShortName+' value is: '+getDROColorCode((item.total).toFixed(0))+' <span style="font-size:13px;text-decoration:underline;text-decoration-style: dotted;">which is the grouping of AR:'+(item.ar).toFixed(0)+' and WIP:'+(item.wip.toFixed(0))+'</span></li>';
                                           //
                                        //
                                                                     });
                                                                        }   
                                                                        else{
                                                                            outline += '<li> '+metricShortName+' value is: '+getColorCode(0)+'</li>';
                                                                        }
                                                                        //  if(pmfinanceData.length > 0){
                                                                        //     pmfinanceData.forEach(function(item){
                                                                        //         outline +='<li> '+metricShortName+' value for pervious month was: '+(item.total).toFixed(0)+' which is '+compareValue(financeData[0].total,item.total)+' this month value.</li>';
                                                                        //     });
                                                                        // }   
                                                                        // else{
                                                                        //      outline +='<li> '+metricShortName+' value for pervious month was: '+(item.total).toFixed(0)+'which is '+compareValue(financeData[0].total,0)+' this month value.</li>';
                                                                        // }
                                                                        // if(pyfinanceData.length > 0){
                                                                        //     pyfinanceData.forEach(function(item){
                                                                        //         outline +='<li> '+metricShortName+' value for last year was: '+(item.total).toFixed(0)+' which is '+compareValue(financeData[0].total,item.total)+' this month value.</li>';
                                                                        //     });
                                                                        // }   
                                                                        // else{
                                                                        //      outline +='<li> '+metricShortName+' value for pervious month was: '+(item.total).toFixed(0)+' which is '+compareValue(financeData[0].total,0)+' this month value.</li>';
                                                                        // }
                                                                       if(pmfinanceData.length > 0 && pyfinanceData.length > 0){
                                                                            outline +='<li>'+getTrend(metricShortName,regionsEntity.resolution.values[0],parseInt((financeData[0].total).toFixed(0)),parseInt((pmfinanceData[0].total).toFixed(0)),parseInt((pyfinanceData[0].total).toFixed(0)))+'</li>';
                                                                       outline+= '</ul>';
                                                                       }
                                                                       //<span style="font-size:13px;text-decoration:underline;text-decoration-style: dotted;">'+metricShortName+' value in last month was '+(pmfinanceData[0].total).toFixed(0)+' and last year was '+(pyfinanceData[0].total).toFixed(0)+' </span>
                                                                       //<span style="font-size:11px;font-style: italic;">The '+metricShortName+' values are on currency USD at 2018 MMC Budget Rate (000\'s).</span>
                                                                       outline+= '&nbsp;<a style="position: absolute;right: 2px;bottom: 2px;" href="javascript:void(0);" onclick=runReport("http://10.52.12.11/MicroStrategy/asp/Main.aspx?evt=2048001&src=Main.aspx.2048001&documentID=302500E0472C9414678D5EA15207DB92&currentViewMedia=1&visMode=0&Server=AUMEL13AS26V&Project=RM&Port=0&share=1&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,footer")>++more</a>';
                                                                            res.json({
                                                                                data:outline
                                                                                });   
                                                                            });
                                                                            });
                                                                        });
                                }
                                else if(regionsEntity){
                                     Store.DROPeriodResponse()
                                    .then(function (array) {
                                        var promptId = 'dro-period-prompt-'+Math.floor(Date.now() / 1000);
                                        var outline ='Please select <bld>Period</bld> for <bld>'+metricShortName+'</bld> -';

                                                outline +='<br><br><table class="table table-bordered" style="background-color:white;"><tbody>';
                                                outline +='<tr><td width="130px"><select id="'+promptId+'" style="margin: 5px;min-width:120px;">';
                                                    //outline+='<option></option>"';
                                        array.forEach(function(item){
                                            outline+='<option>'+item+'</option>"';
                                        });
                                        outline+='</select><input type="button" style="float:right;" class="btn btn-primary btn-flat" onclick=generateDROAnalysis("'+encodeURIComponent(metricShortName)+'","'+encodeURIComponent(regionsEntity.resolution.values[0])+'","'+promptId+'") value="Next" id="dro-period-prompt-selector"></td></tr>';
                                        
                                        outline += '</tbody> </table>';
                                                            res.json({
                                                            data:outline
                                                            });   
                                                        });
                                }
                                else if(periodEntity){
                                       Store.DROAggregateResponse('',periodEntity.resolution.values[0],metricShortName)
                                        .then(function (AggregatedData) {
                                            var monthArr = getMonthArray(periodEntity.resolution.values[0],periodEntity.resolution.values[0]);
                                            Store.DROResponse('',monthArr,metricShortName)//periodEntity.resolution.values[0],metricName)
                                        .then(function (financeData) {
                                        //        Store.DROResponse('',getPreviousMonth(periodEntity.resolution.values[0]),metricName)
                                        // .then(function (pmfinanceData) {
                                        //         Store.DROResponse('',getPreviousYear(periodEntity.resolution.values[0]),metricName)
                                        //  .then(function (pyfinanceData) {
                                            if(financeData.length > 0){
                                                var outline = 'This analysis measures <bld>'+metricShortName+' Trend | As of '+periodEntity.resolution.values[0]+' for all regions.';
                                                     //var outline ='This analysis measures <bld>AR By Region/Business of the Relationship Manager | As of '+periodEntity.resolution.values[0]+'</bld>. The <bld>'+metricShortName+'</bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricShortName+'</bld> for all Business and all Region is ';
                                                    var crossTabArray = [];
                                                     
                                                            financeData.forEach(function(item){
                                                            crossTabArray.push({
                                                                Region: item._id.region,
                                                                [metricShortName]: item._id.period,
                                                                ar: (item.total).toFixed(0)
                                                            });
                                                            });
                                                        outline+= '&nbsp;<a style="position: absolute;right: 2px;bottom: 2px;" href="javascript:void(0);" onclick=runReport("http://10.52.12.11/MicroStrategy/asp/Main.aspx?evt=2048001&src=Main.aspx.2048001&documentID=302500E0472C9414678D5EA15207DB92&currentViewMedia=1&visMode=0&Server=AUMEL13AS26V&Project=RM&Port=0&share=1&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,footer")>++more</a>';
                                                            res.json({
                                                                    data:outline,
                                                                    crossTab: crossTabArray,
                                                                    column:metricShortName,
                                                                    row:'Region',
                                                                    aggregate:'ar',
                                                                    hideTotals: true
                                                                    });  
//                                                 var financeData = [];
//                                                 var row = {
//                                                     region: '',
//                                                     dro:0,
//                                                     pmdro:0,
//                                                     pydro:0
//                                                 };
// // for(i = 0; i <= cmfinanceData.length; i++)
// //     {
// //     row.region = cmfinanceData[i]._id.region;
// //     row.dro = cmfinanceData[i]._id.total;    
// //     row.dro = pmfinanceData[i]._id.total;    
// //         row.dro = pyfinanceData[i]._id.total;    
// //         financeData.push(row);
// //     }
//                                                 var outline = 'This analysis measures <bld>DRO By Region of the Relationship Manager | As of '+periodEntity.resolution.values[0]+'</bld>. The DRO values will be on currency USD at 2018 MMC Budget Rate (000\'s).';
//                                                         //var outline ='This analysis measures <bld>AR By Region/Business of the Relationship Manager | As of '+periodEntity.resolution.values[0]+'</bld>. The <bld>'+metricShortName+'</bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricShortName+'</bld> for Region - <bld>'+regionsEntity.resolution.values[0]+'</bld> and for all Business is ';
//                                                             // AggregatedData.forEach(function(item){
//                                                             //     outline +=getColorCode((item.total).toFixed(0));
//                                                             // });
//                                                             outline +='<br><button class="btn btn-default" style="float: right;padding:0px;margin: 2px 0px;" onclick="quickSwitch('+quickSwitchCount+')"><span id="qs-icon-'+quickSwitchCount+'" style="margin: 5px;" class="fa fa-area-chart"></span></button><button class="btn btn-default" style="float: right;padding:0px;margin: 2px 4px;" onclick="quickExport('+quickSwitchCount+')" title="Export Data"><span id="qs-icon-0" style="margin: 5px;" class="fa fa-cloud-download"></span></button><br><br><div id="qs-container-'+quickSwitchCount+'"><div id="qs-graph-'+quickSwitchCount+'"></div><table  id="qs-table-'+(quickSwitchCount++)+'" class="table table-bordered" style="background-color:white;"><tbody><tr><th>Region</th><th style="width: 135px">'+metricShortName +'</th></tr>';
//                                                             cmfinanceData.forEach(function(item){
//                                                             outline +='<tr><td>'+item._id.region+'</td><td align=right>'+getFormattedCurrency((item.total).toFixed(0))+'</td></tr>';
//                                                             });
//                                                             outline += '</tbody></table></div>';
//                                                             res.json({
//                                                                     data:outline
//                                                                     }); 
                                                                }
                                                                else{
                                                                    res.json({
                                                                        data:'Based on your selection (<bld>'+regionsEntity.resolution.values[0]+'</bld> Region and <bld>'+periodEntity.resolution.values[0]+'</bld> Period), I have not found any data in data store. I have data for Period between <bld>January 2016 to March 2017</bld> and Region - <bld>North America</bld>, <bld>Other</bld>, <bld>EuroPac</bld>, <bld>Growth Markets</bld>.'
                                                                        }); 
                                                                }  
                                         });
                                        // });
                                        // });
                                                                });
                                             
                                }
                                else{
                                    Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });
                                }
                            }
                            else{
                                if(regionsEntity && busniessEntity && periodEntity)
                                {
                                        Store.FinanceResponse(regionsEntity.resolution.values[0],busniessEntity.resolution.values[0],periodEntity.resolution.values[0],metricName)
                                        .then(function (financeData) {
                                                            var outline ='This analysis measures <bld>AR By Region/Business of the Relationship Manager | As of '+periodEntity.resolution.values[0]+'</bld>. The <bld>'+metricShortName+'</bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricShortName+'</bld> for Business - <bld>'+busniessEntity.resolution.values[0]+'</bld> and Region - <bld>'+regionsEntity.resolution.values[0]+'</bld> is ';
                                                            if(financeData.length > 0){
                                                            financeData.forEach(function(item){
                                                                outline +=getColorCode((item.v/1000).toFixed(0));
                                                            });
                                                        }
                                                        else{
                                                            outline +=getColorCode(0);
                                                        }
                                                            res.json({
                                                                    data:outline
                                                                    });   
                                                                });
                                }
                                else if(periodEntity && regionsEntity)
                                {
                                    Store.FinanceAggregateResponse(regionsEntity.resolution.values[0],'',periodEntity.resolution.values[0],metricName)
                                        .then(function (AggregatedData) {
                                            Store.FinanceResponse(regionsEntity.resolution.values[0],'',periodEntity.resolution.values[0],metricName)
                                        .then(function (financeData) {
                                            if(financeData.length > 0){
                                                        var outline ='This analysis measures <bld>AR By Region/Business of the Relationship Manager | As of '+periodEntity.resolution.values[0]+'</bld>. The <bld>'+metricShortName+'</bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricShortName+'</bld> for Region - <bld>'+regionsEntity.resolution.values[0]+'</bld> and for all Business is ';
                                                            AggregatedData.forEach(function(item){
                                                                outline +=getColorCode((item.v/1000).toFixed(0));
                                                            });
                                                            outline +='<br><button class="btn btn-default" style="float: right;padding:0px;margin: 2px 0px;" onclick="quickSwitch('+quickSwitchCount+')" title="Quick Switch"><span id="qs-icon-'+quickSwitchCount+'" style="margin: 5px;" class="fa fa-area-chart"></span></button><button class="btn btn-default" style="float: right;padding:0px;margin: 2px 4px;" onclick="quickExport('+quickSwitchCount+')" title="Export Data"><span id="qs-icon-0" style="margin: 5px;" class="fa fa-cloud-download"></span></button><br><br><div id="qs-container-'+quickSwitchCount+'"><div id="qs-graph-'+quickSwitchCount+'"></div><table  id="qs-table-'+(quickSwitchCount++)+'" class="table table-bordered" style="background-color:white;"><tbody><tr><th>Business</th><th style="width: 135px">'+metricShortName +'</th></tr>';
                                                            financeData.forEach(function(item){
                                                            outline +='<tr><td>'+item._id.business+'</td><td align=right>'+getFormattedCurrency((item.v/1000).toFixed(0))+'</td></tr>';
                                                            });
                                                            outline += '</tbody></table></div>';
                                                            res.json({
                                                                    data:outline
                                                                    }); 
                                                                }
                                                                else{
                                                                    res.json({
                                                                        data:'Based on your selection (<bld>'+regionsEntity.resolution.values[0]+'</bld> Region and <bld>'+periodEntity.resolution.values[0]+'</bld> Period), I have not found any data in data store. I have data for Period between <bld>January 2016 to March 2017</bld> and Region - <bld>North America</bld>, <bld>Other</bld>, <bld>EuroPac</bld>, <bld>Growth Markets</bld>.'
                                                                        }); 
                                                                }  
                                        });
                                                                });
                                                                
                                }
                                else if(periodEntity && busniessEntity)
                                {
                                    Store.FinanceAggregateResponse('',busniessEntity.resolution.values[0],periodEntity.resolution.values[0],metricName)
                                        .then(function (AggregatedData) {
                                                Store.FinanceResponse('',busniessEntity.resolution.values[0],periodEntity.resolution.values[0],metricName)
                                        .then(function (financeData) {
                                            if(financeData.length > 0){
                                                            var outline ='This analysis measures <bld>AR By Region/Business of the Relationship Manager | As of '+periodEntity.resolution.values[0]+'</bld>. The <bld>'+metricShortName+'</bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricShortName+'</bld> for Business - <bld>'+busniessEntity.resolution.values[0]+'</bld> and for all Region is ';
                                                            AggregatedData.forEach(function(item){
                                                                outline +=getColorCode((item.v/1000).toFixed(0));
                                                            });
                                                            outline +='<br><button class="btn btn-default" style="float: right;padding:0px;margin: 2px 0px;" onclick="quickSwitch('+quickSwitchCount+')"><span id="qs-icon-'+quickSwitchCount+'" style="margin: 5px;" class="fa fa-area-chart"></span></button><button class="btn btn-default" style="float: right;padding:0px;margin: 2px 4px;" onclick="quickExport('+quickSwitchCount+')" title="Export Data"><span id="qs-icon-0" style="margin: 5px;" class="fa fa-cloud-download"></span></button><br><br><div id="qs-container-'+quickSwitchCount+'"><div id="qs-graph-'+quickSwitchCount+'"></div><table  id="qs-table-'+(quickSwitchCount++)+'" class="table table-bordered" style="background-color:white;"><tbody><tr><th>Region</th><th style="width: 135px">'+metricShortName +'</th></tr>';
                                                            financeData.forEach(function(item){
                                                            outline +='<tr><td>'+item._id.region+'</td><td align=right>'+getFormattedCurrency((item.v/1000).toFixed(0))+'</td></tr>';
                                                            });
                                                        outline += '</tbody></table></div>';
                                                            res.json({
                                                                    data:outline
                                                                    });  
                                                                }
                                                                else{
                                                                    res.json({
                                                                        data:'Based on your selection (<bld>'+busniessEntity.resolution.values[0]+'</bld> Business and <bld>'+periodEntity.resolution.values[0]+'</bld> Period), I have not found any data in data store. I have data for Period between <bld>January 2016 to March 2017</bld> and Business - <bld>Career</bld>, <bld>Global Business Solutions</bld>, <bld>Wealth</bld>, <bld>Other</bld>, <bld>Mercer Services</bld>, <bld>Health</bld>.'
                                                                        }); 
                                                                }   
                                                                });
                                        });
                                }
                                    else if(periodEntity)
                                {
                                // (periodEntity.resolution.values[0]+' '+metricName);
                                    Store.FinanceAggregateResponse('','',periodEntity.resolution.values[0],metricName)
                                        .then(function (AggregatedData) {
                                                Store.FinanceResponse('','',periodEntity.resolution.values[0],metricName)
                                        .then(function (financeData) {
                                            if(financeData.length > 0){
                                                    var outline ='This analysis measures <bld>AR By Region/Business of the Relationship Manager | As of '+periodEntity.resolution.values[0]+'</bld>. The <bld>'+metricShortName+'</bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricShortName+'</bld> for all Business and all Region is ';
                                                    var crossTabArray = [];
                                                            AggregatedData.forEach(function(item){
                                                                outline +=getColorCode((item.v/1000).toFixed(0));
                                                            });
                                                            financeData.forEach(function(item){
                                                            crossTabArray.push({
                                                                Business: item._id.business,
                                                                Region: item._id.region,
                                                                ar: (item.v/1000).toFixed(0)
                                                            });
                                                            });
                                                            res.json({
                                                                    data:outline,
                                                                    crossTab: crossTabArray,
                                                                    column:'Region',
                                                                    row:'Business',
                                                                    aggregate:'ar'
                                                                    });   
                                                                }
                                                                else{
                                                                    res.json({
                                                                        data:'Based on your selection ( <bld>'+periodEntity.resolution.values[0]+'</bld> Period), I have not found any data in data store. I have data for Period between <bld>January 2016 to March 2017</bld>.'
                                                                        }); 
                                                                }   
                                                                });
                                        });
                                }
                                        else if(busniessEntity)
                                {
                                // (periodEntity.resolution.values[0]+' '+metricName);
                                    Store.FinanceAggregateResponse('',busniessEntity.resolution.values[0],'',metricName)
                                        .then(function (AggregatedData) {
                                                Store.FinanceResponse('',busniessEntity.resolution.values[0],'',metricName)
                                        .then(function (financeData) {
                                            if(financeData.length > 0){
                                                    var outline ='This analysis measures <bld>AR By Business of the Relationship Manager | As of '+busniessEntity.resolution.values[0]+'</bld>. The <bld>'+metricShortName+'</bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricShortName+'</bld> for all Region and all Period is ';
                                                    var crossTabArray = [];
                                                            AggregatedData.forEach(function(item){
                                                                outline +=getColorCode((item.v/1000).toFixed(0));
                                                            });
                                                            financeData.forEach(function(item){
                                                            crossTabArray.push({
                                                                Period: item._id.period,
                                                                Region: item._id.region,
                                                                ar: (item.v/1000).toFixed(0)
                                                            });
                                                            });
                                                            res.json({
                                                                    data:outline,
                                                                    crossTab: crossTabArray,
                                                                    column:'Region',
                                                                    row:'Period',
                                                                    aggregate:'ar'
                                                                    });
                                                                }
                                                                else{
                                                                    res.json({
                                                                        data:'Based on your selection ( <bld>'+busniessEntity.resolution.values[0]+'</bld> Business), I have not found any data in data store. I have data for following Business - </ br><bld>Career</bld>, <bld>Global Business Solutions</bld>, <bld>Wealth</bld>, <bld>Other</bld>, <bld>Mercer Services</bld>, <bld>Health</bld>'
                                                                        }); 
                                                                }      
                                                                });
                                        });
                                }
                                        else if(regionsEntity)
                                {
                                // (periodEntity.resolution.values[0]+' '+metricName);
                                    Store.FinanceAggregateResponse(regionsEntity.resolution.values[0],'','',metricName)
                                        .then(function (AggregatedData) {
                                                Store.FinanceResponse(regionsEntity.resolution.values[0],'','',metricName)
                                        .then(function (financeData) {
                                            if(financeData.length > 0){
                                                    var outline ='This analysis measures <bld>AR By Region of the Relationship Manager | As of '+regionsEntity.resolution.values[0]+'</bld>. The <bld>'+metricShortName+'</bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricShortName+'</bld> for all Business and all Period is ';
                                                    var crossTabArray = [];
                                                            AggregatedData.forEach(function(item){
                                                                outline +=getColorCode((item.v/1000).toFixed(0));
                                                            });
                                                            financeData.forEach(function(item){
                                                            crossTabArray.push({
                                                                Period: item._id.period,
                                                                Business: item._id.business,
                                                                ar: (item.v/1000).toFixed(0)
                                                            });
                                                            });
                                                            res.json({
                                                                    data:outline,
                                                                    crossTab: crossTabArray,
                                                                    column:'Business',
                                                                    row:'Period',
                                                                    aggregate:'ar'
                                                                    });  
                                                                }
                                                                else{
                                                                    res.json({
                                                                        data:'Based on your selection (<bld>'+regionsEntity.resolution.values[0]+'</bld> Region), I have not found any data in data store. I have data for following Region - </br><bld>North America</bld>, <bld>Other</bld>, <bld>EuroPac</bld>, <bld>Growth Markets</bld>'
                                                                        }); 
                                                                }     
                                                                });
                                        });
                                }
                                    else
                                    {               
                                    Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                }); 
                                    }
                                }
                        }
                                else
                                {               
                                 Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });
                                }
                        break;
                        case 'SuggestAPACDimensions':
                          var FinanceMetricsEntity =result.entities[result.entities.findIndex(function(val) {
                                      return val.type == 'FinanceMetrics'
                                    })];
                                if(FinanceMetricsEntity){
                                     var outline ='Based on the data available in Enterprise Reporting development environment for data-mining i.e. upto March 2017, here is the summarized verdict towards <bld>"Seasonal Prediction"</bld>:<br/><ul type="disc"><li><bld>Prediction Model</bld></li><ul type="circle"><li>Time Series Predictive Model</li></ul><li><bld>Narrative Verdict</bld></li><ul type="circle"><li>We have processed historical record of 3 years (2015, 2016, 2017) for business and geographies.</li><li>We analyized <bld>Current Year Revenue</bld> for business (Health, Career, Global Business Solutions and Others) and geographies (Europac, Growth Markets and North America).</li><li>Timeline forecast predicted CYR trending for next <bld>10</bld> months.</li></ul><li><bld>Conclusion</bld></li><ul type="circle"><li>Prediction shows a continuous trend ranging from <bld><grn>144.3%</grn></bld> (for Jan 2016) to <bld><rd>94.3%</rd></bld> (for Aug 2017).</li></ul></ul>';
                            outline +="<a id='fancy-box'  style='position: absolute;right:60px;' href='./img/prediction.png' title='This image depicts <bld>Seasonal Prediction</bld> for <bld>Current Year Revenue</bld>.'><img alt='Seasonal Prediction' src='' tile=''/></a>&nbsp;";
                             outline+= '&nbsp;<a style="position: absolute;right: 2px;bottom: 2px;" href="javascript:void(0);" onclick=runReport("http://10.197.135.69/MicroStrategy/asp/Main.aspx?evt=3140&src=Main.aspx.3140&documentID=96AB04794071BD346179CC9E952ADDFD&Server=INGGNED8N3YQG2&Project=MicroStrategy%20Tutorial&Port=0&share=1&uid=administrator&pwd=ladmin&hiddensections=header,path,dockTop,footer")>++more</a>';
                                        res.json({
                                        data:outline
                                    });  
                                }
                                else{
                                     Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });
                                }
                        break;
                        default:
                               Store.saveUtterance(queryData)
                                  .then(function () { res.json({
                                        data:'As of now, I am not trained to cater this request. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                                                               });
                                });
                        break;
                    }
                });
    }
    else{
         Store.getFAQIntent(req.params.query)
                .then(function (result) {
                    var result = utterances.filter(function(utterance){
                return (utterance.intentName == result.topScoringIntent.intent);
                });
                if(result.length > 0){
                res.json({
                    data:result[0].answer
                });
                }
            else{
                res.json({
                data:'As of now, I don\'t have this information in my FAQ list. Please reach out to <a href="mailto:pankaj.rautela@mercer.com">administrator</a>'
                });
            }
        });
          
    }
}

var reportList = function(req, res, next) {
                
                if(req.params.project == 'People_Productivity'){
                    var outline ='As per your selection, in <bld>'+req.params.project+'</bld> project I have found below reports.<br/>You may execute any report by clicking report-name.';
                    outline += '<br/>1. <a href="javascript:void(0);" onclick=runReport("https://mercer.cloud.microstrategy.com:1443/MicroStrategy/servlet/mstrWeb?Server=C333LIWD03NA01&Project=People+Productivity&Port=0&evt=2048001&src=mstrWeb.2048001&visMode=0&currentViewMedia=2&documentID=6543312846652B1B671E488BE5CF9D3D")>Monthly Time Entry Compliance Dashboard</a>';

                       outline += '<br/>2. <a href="javascript:void(0);" onclick=runReport("https://mercer.cloud.microstrategy.com:1443/MicroStrategy/servlet/mstrWeb?Server=C333LIWD03NA01&Project=People+Productivity&Port=0&evt=2048001&src=mstrWeb.2048001&visMode=0&currentViewMedia=2&documentID=9A657DB84474F4FCE0A4F7A1CD74D9C8")>IT Monthly Time Entry Compliance Dashboard</a>';
                        res.json({
                        data:outline
                    }); 
                     
                }
                else if(req.params.project == 'Client_Profitablity'){
                    var outline ='As per your selection, in <bld>'+req.params.project+'</bld> project I have found below reports.<br/>You may execute any report by clicking report-name.';
                      outline += '<br/>1. <a href="javascript:void(0);" onclick=runReport("https://mercer.cloud.microstrategy.com:1443/MicroStrategy/servlet/mstrWeb?Server=C333LIWD03NA01&Project=Sales+Operations&Port=0&evt=2048001&src=mstrWeb.2048001&visMode=0&currentViewMedia=2&documentID=2210D87C43983ECCCE53DDA9B26CDE81")>Leadership Sales Dashboard</a>';
                      outline += '<br/>2. <a href="javascript:void(0);" onclick=runReport("https://mercer.cloud.microstrategy.com:6443/MicroStrategy/servlet/mstrWeb?Server=C333LIWD03NA01&Project=Client+Profitability&Port=0&evt=2048001&src=mstrWeb.2048001&visMode=0&currentViewMedia=2&documentID=9DC8B0E74CFBFD06DF28BBB2F4360622")>AR & WIP Dashboard</a>';
                      outline += '<br/>3. <a href="javascript:void(0);" onclick="selectPeriodPrompt()">AR Daily Dashboard</a>';
                      res.json({
                        data:outline
                    });   
                }
                else{
                    Store.searchReports(req.params.project)
                    .then(function (reports) {
                        var outline ='As per your selection, in <bld>'+req.params.project+'</bld> project I have found below reports.<br/>You may execute any report by clicking report-name.';
                        reports.forEach(function(val,index){
                            outline += '<br/>'+(index+1)+'. <a href="javascript:void(0);" onclick=runReport("http://aumel13as26v/microstrategy/asp/Main.aspx?Server=-Hvj-9FOGThHtL_gNFK0kvldLjnU%3D&Project=APAC_CAD_Analytics%28China%29&Port=-P4NgwgBR5B0OtjIg&evt=4001&src=Main.aspx.4001&visMode=0&reportViewMode=2&reportID='+val.ReportGUID+'&reportSubtype=769&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,dockLeft,footer")>'+val.ReportName+'</a>';
                        });  
                        res.json({
                            data:outline
                        });   
                    });
                }
}

var GenerateDROAnalysis = function(req, res, next) {
//   Store.DROResponse(decodeURIComponent(req.params.region),req.params.period,req.params.metric)
//                                         .then(function (financeData) {
//                                                                            var outline = 'This analysis measures <bld>DRO By Region of the Relationship Manager | As of '+req.params.period+' for region '+req.params.region+'</bld>. The DRO values will be on currency USD at 2018 MMC Budget Rate (000\'s).';
//                                                                         if(financeData.length > 0){
//                                                                             financeData.forEach(function(item){
//                                                                                 outline +=getColorCode((item.total).toFixed(0));
//                                                                             });
//                                                                         }   
//                                                                         else{
//                                                                             outline +=getColorCode(0);
//                                                                         }
//                                                                             res.json({
//                                                                                 data:outline
//                                                                                 });   
//                                                                             });
Store.DROResponse(decodeURIComponent(req.params.region),decodeURIComponent(req.params.period),decodeURIComponent(req.params.metric))
                                        .then(function (financeData) {
                                            
                                                     Store.DROResponse(decodeURIComponent(req.params.region),getPreviousMonth(decodeURIComponent(req.params.period)),decodeURIComponent(req.params.metric))
                                        .then(function (pmfinanceData) {
                                                Store.DROResponse(decodeURIComponent(req.params.region),getPreviousYear(decodeURIComponent(req.params.period)),decodeURIComponent(req.params.metric))
                                         .then(function (pyfinanceData) {
                                            
                                                                           var outline = 'This analysis measures <bld>'+decodeURIComponent(req.params.metric)+' Trend | As of '+decodeURIComponent(req.params.period)+' for region '+decodeURIComponent(req.params.region)+'</bld>.<br>As per my analysis - <ul type="disc">';
                                                                        if(financeData.length > 0){
                                                                            financeData.forEach(function(item){
                                                                                outline +='<li> '+decodeURIComponent(req.params.metric)+' value is: '+getDROColorCode((item.total).toFixed(0))+' <span style="font-size:13px;text-decoration:underline;text-decoration-style: dotted;">which is the grouping of AR:'+(item.ar).toFixed(0)+' and WIP:'+(item.wip.toFixed(0))+'</span></li>';
                                                                     });
                                                                        }   
                                                                        else{
                                                                            outline += '<li> '+decodeURIComponent(req.params.metric)+' value is: '+getColorCode(0)+'</li>';
                                                                        }
                                                                          if(pmfinanceData.length > 0 && pyfinanceData.length > 0){
                                                                            outline +='<li>'+getTrend(decodeURIComponent(req.params.metric),decodeURIComponent(req.params.region),(financeData[0].total).toFixed(0),(pmfinanceData[0].total).toFixed(0),(pyfinanceData[0].total).toFixed(0))+'</li>';
                                                                       outline+= '</ul>';

                                                                          }
                                                                                                                                           outline+= '&nbsp;<a style="position: absolute;right: 2px;bottom: 2px;" href="javascript:void(0);" onclick=runReport("http://10.52.12.11/MicroStrategy/asp/Main.aspx?evt=2048001&src=Main.aspx.2048001&documentID=302500E0472C9414678D5EA15207DB92&currentViewMedia=1&visMode=0&Server=AUMEL13AS26V&Project=RM&Port=0&share=1&uid=administrator&pwd=ladmin&hiddensections=header,path,dockTop,footer")>++more</a>';
                                                                            res.json({
                                                                                data:outline
                                                                                });   
                                                                            });
                                                                            });
                                                                        });
}

var ReportsByMetric = function(req, res, next) {
       Store.ReportListByMetricResponse(req.params.MetricName)
        .then(function (reports) {
                var outline ='As per your selection, for <bld>'+toTitleCase(req.params.MetricName)+'</bld> metric. I have found below reports.<br/>You may execute any report by clicking report-name.';
                reports.forEach(function(val,index){
                    outline += '<br/>'+(index+1)+'. <a href="javascript:void(0);" onclick=runReport("http://10.197.132.81/microstrategy/asp/Main.aspx?Server=-Hvj-9FOGThHtL_gNFK0kvldLjnU%3D&Project=APAC_CAD_Analytics%28China%29&Port=-P4NgwgBR5B0OtjIg&evt=4001&src=Main.aspx.4001&visMode=0&reportViewMode=2&reportID='+val.ReportGUID+'&reportSubtype=769&uid=administrator&pwd=madmin&hiddensections=header,path,dockTop,dockLeft,footer")>'+val.ReportName+'</a>';
                   
                });
                  res.json({
                            data:outline
                        });   
                });
}

var TalentReports = function(req, res, next) {
       var outline ='As per your selection, in <bld>'+req.params.ProjectName+'</bld> project, I have found below reports.<br/>You may execute any report by clicking report-name.';
            if(req.params.ProjectName == 'Human_Resources'){
                    outline += '<br/><ul><li><a href="javascript:void(0);" onclick=ExecuteReport("Gender_diversity_ratio")>Gender Diversity Ratio</a></li>';
                    outline += '<li><a href="javascript:void(0);" onclick=ExecuteReport("employee_headcount")>Employee Headcount</a></li>';
                    outline += '<li><a href="javascript:void(0);" onclick=ExecuteReport("tiers_analysis")>Tiers Analysis</a></li></ul>';
                }
                else if(req.params.ProjectName == 'Learning&Development'){
 outline += '<br/><ul><li><a href="javascript:void(0);" onclick=ExecuteReport("MercerOS_Charter")>On-Demand MercerOS Charter</a></li></ul>';
                }
                  res.json({
                            data:outline
                        });   
 }
var refreshDataSet = function(req, res, next) {
    //var id = "CB12C2844E81CE71ECBB8D8193468BE2";
    // req.params.CubeId
    // req.params.CollectionName
    JsonAPI.getSession().then(function(session){
    JsonAPI.getCubeData(session.authToken,req.params.CubeId)
    .then(function(result){
      Store.deleteCubeData(req.params.CollectionName)
                .then(function () {
                    Store.insertCubeData(result,req.params.CollectionName)
                    .then(function () {
                        Store.UpdateMetaDataResponse(req.params.CubeId)
                            .then(function (metadata) {
                                            res.json({
                                                        status:"complete"
                                                    });   
                        });  
                });
            });
        }); 
    });
}
var FAQ  = function(req, res, next) {
     Store.FAQListResponse(req.params.faqCategory)
            .then(function (list) {
                          res.json({
                        data:list
                    });   
                });
}
var CubeInformation = function(req, res, next) {
    //req.params.CubeName 
    var outline = '<bld>Narrative Summary</bld> of <bld>'+req.params.CubeName+'</bld> cube.<ul><li>This Cube contains two object prompts (ClaimsDimsOP, ClaimsMetricsOP) to facilitate on the fly creation of report template.</li><li>This Cube have information about "Paid Claims" and associated "Number of Claimants" metrics at following level.<ul type="circle"><li>Country-CHN</li><li>Entity</li><li>Group Name(U)</li><li>Mercer Product Class 1</li><li>Mercer Product Class 2</li><li>Policy</li><li>Policy-Period</li></ul></li>';

    outline+= '&nbsp;<a style="position: absolute;right: 2px;bottom: 2px;" target="_blank" href="http://10.197.132.81/MicroStrategy/asp/Main.aspx?evt=4001&src=Main.aspx.4001&visMode=0&reportViewMode=1&reportID=298454CE47399499627869B252F23DDE&reportSubtype=768&Server=10.52.12.11&Project=APAC_CAD_Analytics(China)&Port=0&objectsPromptAnswers=4ADB85AD4732080C3646CD847975B1D3~12~[Country-CHN]%1BD051472444C75396ACDF2695CCC975E7~12~[Entity]%1B5EB70E2448C3154372DB53A80A3CA879~12~[Group Name(U)]%1BD159CC4F46FAF7DC1A260F873AD58117~12~[Mercer Product Class 1]%1BFCB5D7E243BF5C0724C38F9B54554D3B~12~[Mercer Product Class 2]%1BA44E4F3D4160C6E8A89EC3A504A5CC3F~12~[Policy]%1B063B4F654B6CCA8DF98623A98435A86E~12~[Policy-Period]%5E9958C203400827B23FA623AC400A712D~4~[#Claimants]%1B950D383D41D2AC0EC66F939D5BC37861~4~[Paid Claims]&uid=pankaj&pwd=pankaj1&hiddensections=header,path,dockTop,dockLeft,footer">++more</a>'
        res.json({
                data:outline
            });   
 }

var ExecuteReport = function(req, res, next) {
    var outline = '&nbsp;';
     if(req.params.ReportName == 'tiers_analysis'){
         outline ="<a id='fancy-box' href='./img/Tiers.jpg' title='This image depicts Tier wise span analysis for Mercer GTI and PM vs IC ratio for September, 2017.'><img alt='example5' src='./img/Tiers.jpg' style='width:190px;height:100px;' title='Click here to see large view'/></a>";
     }
        else if(req.params.ReportName == 'Gender_diversity_ratio'){
         outline ="<a id='fancy-box' href='./img/gender.jpg' title='This image depicts gender diversity ratio across GTI by level 7 for September, 2017.'><img alt='example5' src='./img/gender.jpg' style='width:185px;height:100px;'  title='Click here to see large view'/></a>";
     }
          else if(req.params.ReportName == 'employee_headcount'){  
         outline ="<a id='fancy-box' href='./img/headcount.png' title='This image depicts Employee headcount for Mercer GTI by LOB for September, 2017.'><img alt='example5' src='./img/headcount.png' style='width:200px;height:100px;' title='Click here to see large view'/></a>";
     }
             else if(req.params.ReportName == 'MercerOS_Charter'){
         outline ="<a id='fancy-box' href='./img/MercerOSCharter.png' title='This image depicts MercerOS status Charter for March, 2017. This is available on demand.'><img alt='example5' src='./img/MercerOSCharter.png' style='width:200px;height:100px;' title='Click here to see large view'/></a>";
     }
                  res.json({
                            data:outline
                        });   
 }

var promptAnalysis = function(req, res, next) {
       Store.PromptResponse(req.params.promptType)
                       .then(function (array) {
                           var outline ='Please select prompt answer for <bld>';
                            if(req.params.promptType == 3){
                        outline += '<span class="badge bg-orange">'+req.params.promptName+'</span>';
                            }
                    else if(req.params.promptType == 2){
                        outline += '<span class="badge bg-purple">'+req.params.promptName+'</span>';
                            }
                    else if(req.params.promptType == 1){
                        outline += '<span class="badge bg-blue">'+req.params.promptName+'</span>';
                            }

                              if(req.params.promptType == 3){
                                  outline +='</bld> prompt -<br><br><table class="table table-bordered" style="background-color:white;"><tbody>';
                                   outline +='<tr><td width="130px"><select id="prompt-'+Math.floor(Date.now() / 1000)+'" style="margin: 5px;min-width:120px;">';
                                     //outline+='<option></option>"';
                          array.forEach(function(item){
                            outline+='<option>'+item+'</option>"';
                          });
                        outline+='</select><input type="button" style="float:right;" class="btn btn-primary btn-flat" onclick=selectRegionPrompt("prompt-'+Math.floor(Date.now() / 1000)+'") value="Next" id="period-prompt"></td></tr>';
                              }
                              else{
                          outline +='</bld> prompt -<br><br><table class="table table-bordered" style="background-color:white;"><tbody>';
                          array.forEach(function(item){
                            outline+='<tr><td width="130px"><label><input type="checkbox" value="'+item+'" class="prompt-'+Math.floor(Date.now() / 1000)+'"> '+item+'</label></td></tr>';
                          });
                              }
        //                 if(req.params.promptType == 3){
        //  outline += '<tr><td width="130px"><input type="button" class="btn btn-primary btn-flat" onclick=selectRegionPrompt("prompt-'+Math.floor(Date.now() / 1000)+'") value="Next" id="period-prompt"></td></tr>';
        //                 }
         if(req.params.promptType == 2){
         outline += '<tr><td width="130px"><input type="button" class="btn btn-primary btn-flat" onclick=selectBusinessPrompt("prompt-'+Math.floor(Date.now() / 1000)+'") value="Next" id="region-prompt"></td></tr>';
                        }
        else if(req.params.promptType == 1){
         outline += '<tr><td width="130px"><input type="button" class="btn btn-primary btn-flat" onclick=generateAnalysis("prompt-'+Math.floor(Date.now() / 1000)+'") value="Generate Analysis" id="business-prompt"></td></tr>';
                        }
         
         outline += '</tbody> </table>';
                             res.json({
                            data:outline
                            });   
                        });
}
    function getFromattedParam(param) {
        if(param.indexOf(',') > -1){

           return param.replace(/,/g,',').split(',');
            // param = '\''+param;
            // //param = param.replace(',', '","');
            // param = param.split(',').join('\',\'');
            // param = param +'\'';
        }
        else if(param){
             var array = [];
                    while(array.length > 0) {
            array.pop();
            }
            array.push(param);
            return array;
        }
        else{
            var array = [];
                    while(array.length > 0) {
            array.pop();
            }
            array.push(param);
            return array;
        }
        //return param;
    }
       function formatOutput(param) {
      if(param.indexOf(',') > -1){
           return param.replace(/,/g,', ').split(',');
            // param = '\''+param;
            // //param = param.replace(',', '","');
            // param = param.split(',').join('\',\'');
            // param = param +'\'';
        }
        else if(param){
             var array = [];
                    while(array.length > 0) {
            array.pop();
            }
            array.push(param);
            return array;
        }
        else{
            var array = [];
                    while(array.length > 0) {
            array.pop();
            }
            array.push('All');
            return array;
        }
        //return param;
    }
    var regionPrompt = '', businessPrompt = '', periodPrompt = '';
var generateAnalysis = function(req, res, next) {
    if(req.params.region){
            regionPrompt = req.params.region.toString();
    }
        if(req.params.business){
            businessPrompt = req.params.business.toString();
    }
        if(req.params.period){
            periodPrompt = req.params.period.toString();
    }
    Store.FinanceResponseWithMultiplePrompts(getFromattedParam(regionPrompt),getFromattedParam(businessPrompt),getFromattedParam(periodPrompt),'$AR Outstandingv')
                                      .then(function (financeData) {
                                                       // var outline ='This analysis measures <bld>AR By Region/Business of the Relationship Manager | As of </bld>. The <bld></bld> values will be on currency USD at 2018 MMC Budget Rate (000\'s). Value of <bld>'+metricsEntity.entity+'</bld> for Business - <bld>'+busniessEntity.resolution.values[0]+'</bld> and Region - <bld>'+regionsEntity.resolution.values[0]+'</bld> is ';
                                                       var outline ='<table><tr><td>The <bld>AR Outstanding</bld> values will be on currency USD at 2017 MMC Budget Rate (000\'s).</td><td valign="top"><button title="Re-Prompt" type="button" class="btn btn-box-tool" data-widget="refresh" style="float: right;" id="btn-refresh" onclick="selectPeriodPrompt()"><i class="fa fa-refresh"></i></button></td></tr></table>For selected prompt answers<br>';
                                                       outline += '<span class="badge bg-orange">Period:</span> '+formatOutput(periodPrompt)+'<br>';
                                                       outline += '<span class="badge bg-purple">Region:</span> '+formatOutput(regionPrompt)+'<br>';
                                                       outline += '<span class="badge bg-blue">Business:</span> '+formatOutput(businessPrompt)+'<br>';
                                                    outline += 'Total <bld>Accounts Receivable</bld> value is: ';
                                                       var totalAmount = 0;
                                                        financeData.forEach(function(item){
                                                            totalAmount =parseInt(totalAmount) + parseInt((item.v/1000).toFixed(0));
                                                            
                                                        });
                                                        outline +=getColorCode(totalAmount);
                                                        res.json({
                                                                data:outline
                                                                });   
                                                            });
  
}               
var permiumAnalysis = function(req, res, next) {
       Store.PremiumResponse(req.params.premiumType)
                       .then(function (premiums) {
                        var aray = [];
                        premiums.forEach(function(item){
                            aray.push(item.Value);
                        });
                        var output = maxMinAvg(aray);
                                    var maxIndex = premiums.findIndex(function(val) {
                                      return val.Value == output[0]
                                    });
                                    var minIndex = premiums.findIndex(function(val) {
                                      return val.Value == output[1]
                                    });
                        var outline ='This analysis measures Premium Amount by '+req.params.premiumType+'.<br/><ul type="disc"><li>Total Aggregated Premium across <bld>'+premiums.length+'</bld> entites is <bld>$'+output[1]+'</bld> which averages to <bld>$'+Math.round(output[3],0)+'</bld>.</li><li> The distribution ranges from <bld>$'+premiums[minIndex].Value+'</bld> ('+premiums[minIndex].Header+') to  <bld>$'+premiums[maxIndex].Value+'</bld> ('+premiums[maxIndex].Header+'), a difference of <bld>$'+(premiums[maxIndex].Value-premiums[minIndex].Value+'</bld>.</li></ul>');
                             res.json({
                            data:outline
                            });   
                        });
}
var claimAnalysis =function(req, res, next) {
       Store.ClaimsResponse(req.params.claimType)
                        .then(function (claims) {
                        var aray = [];
                        claims.forEach(function(item){
                            aray.push(item.Value);
                        });
                        var output = maxMinAvg(aray);
                                    var maxIndex = claims.findIndex(function(val) {
                                      return val.Value == output[0]
                                    });
                                    var minIndex = claims.findIndex(function(val) {
                                      return val.Value == output[1]
                                    });
                    
                        var outline ='This analysis measures Claims Amount by '+req.params.claimType+'. <br/><ul type="disc"><li>Total Aggregated Claims across <bld>'+claims.length+'</bld> entites is <bld>$'+output[2]+'</bld> which averages to <bld>$'+(Math.round(output[2]/aray.length,0))+'</bld>.</li><li> The distribution ranges from <bld>$'+claims[minIndex].Value+'</bld> ('+claims[minIndex].Header+') to  <bld>$'+claims[maxIndex].Value+'</bld> ('+claims[maxIndex].Header+'), a difference of <bld>$'+(claims[maxIndex].Value-claims[minIndex].Value+'</bld>.</li></ul>');
                         res.json({
                            data:outline
                            });   
                        });
}

var addLuisApp = function(req, res, next){
        Store.addLuisAppResponse(req.params.appName, req.params.AppId)
                        .then(function (metadata) {
                            res.json({
                                        status:"complete"
                                    });   
                        });  
}
var getLuisApps = function(req, res, next){
       Store.getLuisAppsResponse()
                        .then(function (collection) {
                        res.json({
                            data:collection
                            });   
                        });
}
var getUnmappedUtterances = function(req, res, next){
       Store.getUnmappedUtterances()
                        .then(function (collection) {
                        res.json({
                            data:collection
                            });   
                        });
}

server.post('/api/AddLuisApp', addLuisApp);
server.post('/api/GetLuisApps', getLuisApps);
server.post('/api/GetUnmappedUtterances',getUnmappedUtterances);
server.post('/api/chat', webChat);
server.post('/api/ReportList', reportList);
server.post('/api/ClaimAnalysis', claimAnalysis);
server.post('/api/PromptAnalysis', promptAnalysis);
server.post('/api/PermiumAnalysis', permiumAnalysis);
server.post('/api/GenerateAnalysis', generateAnalysis);
server.post('/api/ReportsByMetric', ReportsByMetric);
server.post('/api/GenerateDROAnalysis', GenerateDROAnalysis);
server.post('/api/TalentReports', TalentReports);
server.post('/api/ExecuteReport', ExecuteReport);
server.post('/api/CubeInformation', CubeInformation);
server.post('/api/FAQ', FAQ);
server.post('/api/RefreshDataSet', refreshDataSet);

var ratingCount = 0;
var quickSwitchCount = 0;
var carouselContainer = 1;
function maxMinAvg(arr) {
    var max = arr[0];
    var min = arr[0];
    var sum = arr[0];
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
        if (arr[i] < min) {
            min = arr[i];
        }
        sum = sum + arr[i];
    }
    return [max, min, sum, sum/arr.length]; 
} 
var monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

function getPreviousMonth(period){
     var arr = period.split(' ');
 if(monthArray[monthArray.indexOf(arr[0])] == 'Jan'){
  return monthArray[monthArray.length-1]+' ' +(parseInt(arr[1])-1);
 }
 else{
    return monthArray[monthArray.indexOf(arr[0])-1]+' ' +arr[1];
 }
}
function getMonthArray(period){
    // temp array
    var output = [];

    //tm
    output.push(period);

    //lm
 var arr = period.split(' ');
 if(monthArray[monthArray.indexOf(arr[0])] == 'Jan'){
  output.push(monthArray[monthArray.length-1]+' ' +(parseInt(arr[1])-1));
 }
 else{
    output.push(monthArray[monthArray.indexOf(arr[0])-1]+' ' +arr[1]);
 }
    //lylm
     var arr1 = period.split(' ');
    output.push(arr1[0]+' '+(parseInt(arr1[1])-1));

    return output;
}

function getPreviousYear(period){
    var arr = period.split(' ');
    return arr[0]+' '+(parseInt(arr[1])-1);//  monthArray[monthArray.indexOf(arr[0])-1]+' ' +arr[1];
}

function getColorCode(val){
    if(val>100){
        return '<span class="badge bg-green">'+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>';
    }
    else if(val>80){
        return '<span class="badge bg-orange">'+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>';
    }
    else{
        return '<span class="badge bg-red">'+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>';
    }
}

function getDROColorCode(val){
    if(val>88){
        return '<span class="badge bg-red">'+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>';
    }
    else if(val>=84){
        return '<span class="badge bg-yellow">'+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>';
    }
    else{
        return '<span class="badge bg-green">'+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</span>';        
    }
}


function getFormattedCurrency(val){
        // return '$ '+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function compareValue(first,last){
    console.log(first);
    console.log(last);
    if(first == last){
return 'equals to';
    }else if(first > last){
return 'greater than';
    }else{
return 'less than';
    }
}
//// In summary DRO is moving up/ down across region/ the board since last month, but/ which is still higher/ lower than last year.
function getTrend(metric,region,first,previous,last){
    console.log(first+' '+previous+' '+last);
    var summary = ''+metric+' value is ';
    if((first == previous) &&  (first == last)){
        summary += 'equal with last month value ('+previous+') and last year value ('+last+') across '+region+'.';
    }
    else if((first == previous) && (first > last)){
        summary += 'equal to last month value ('+previous+'), but is still higher than last year value ('+last+') across '+region+'.';
    }
    else if((first == previous) && (first < last)){
        summary += 'equal to last month value ('+previous+'), but is still lower than last year value ('+last+') across '+region+'.';
    }
    else if((first > previous) && (first > last)){
        summary += 'moving up across '+region+' since last month value ('+previous+'), which is also higher than last year value ('+last+').';
    }
    else if((first > previous) && (first < last)){
        summary += 'moving up across '+region+' since last month value ('+previous+'), but still  is lower than last year value ('+last+').';
    }
     else if((first < previous) && (first > last)){
        summary += 'moving down across '+region+' since last month value ('+previous+'), which is still higher than last year value ('+last+').';
    }
    else if((first < previous) && (first < last)){
        summary += 'moving down across '+region+' since last month value ('+previous+'), which is also lower than last year value ('+last+').';
    }
    else if((first < previous) && (first == last)){
        summary += 'moving down across '+region+' since last month value ('+previous+'), but is equals to last year value ('+last+').';
    }
     else if((first > previous) && (first == last)){
        summary += 'moving up across '+region+' since last month value ('+previous+'), but is equals to last year value ('+last+').';
    }
    return summary;
}
function getSqlCondition(metricName,filter,value){
var columnName = metricName == 'CYR'?'Current_Year_Revenue':'Total_Opp_revenue';
    switch(filter){
        case 'less than':
        return columnName +'<'+ (value/100)
        break;
        case 'less than equals to':
        return columnName +'<='+ (value/100)
        break;
        case 'less than equal to':
        return columnName +'<='+ (value/100)
        break;
        case 'greater than':
        return columnName +'>'+ (value/100)
        break;
        case 'greater than equals to':
        return columnName +'>='+ (value/100)
        break;
        case 'greater than equal to':
        return columnName +'>='+ (value/100)
        break;
        default:
        return ''
        break;
    }
}