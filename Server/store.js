var Promise = require('bluebird');

var request = require('request-promise');
var querystring = require('querystring');
var http = require('http');

//var collectionName = 'dro';//dro'; //arwip, arap
//var mongo = require('./mongo');
var MongoClient = require('mongodb').MongoClient;
//var dbPath = 'mongodb://localhost:27017/cad';
var dbPath = 'mongodb://localhost:27017/APACDemo';
var dbName = 'APACDemo';
var totalAR, totalWIP, addmetric;

var sql = require('mssql');
var config = {
    user: process.env.USER,
    password: process.env.PASSWORD,
    server: process.env.SERVER,
    database: process.env.DATABASE,
    port: process.env.SQLPORT
};

function getSqlData(sqlQuery){
     return new Promise(function(resolve, reject) {
        sql.close();
        sql.connect(config, function (err) {
            if (err) reject(err);
            
            var request = new sql.Request();

            request.query(sqlQuery, function (err, resultSet) {
                if (err) reject(err);
                
                resolve(resultSet.recordsets[0]);
            });
        });
    });
}

// Get ProjectList
function getProjectList(){
    return new Promise(function(resolve, reject) {
        var projects = [];
        var extServerOptions = {
        host: 'localhost',
        port:80,
        headers: {
            'Content-Type': 'application/json'
        },
        path: '/MicrostrategyMVC/metadata/GetProjectList?ProjectKey=0',
        method: 'GET'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
        res.on('data', function(data) {
        responseString += data;
        });
        res.on('end', function() {
        var responseObject = JSON.parse(responseString);
        responseObject.forEach(function(val){
        projects.push(val.IS_PROJ_NAME);
        });
        resolve(projects);
        });
    }).end();
  });
}
//Get ReportList
function getReportList(projectName){
    return new Promise(function(resolve, reject) {
        var reports = [];
        var extServerOptions = {
        host: 'localhost',
        port:80,
        headers: {
            'Content-Type': 'application/json'
        },
        path: '/MicrostrategyMVC/metadata/GetReportList?ProjectName='+projectName,
        method: 'GET'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
            res.on('data', function(data) {
            responseString += data;
        });
        res.on('end', function() {
            var responseObject = JSON.parse(responseString);
            responseObject.forEach(function(val){
            //reports.push(val.IS_DOC_NAME);
            reports.push({'ReportName':val.IS_DOC_NAME,
            'ReportGUID':val.IS_DOC_ID});
            });
            resolve(reports);
            });
        }).end();
    });
}
function getFAQList(faqCategory){
   return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
          resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('FAQ');
        collection.aggregate([{$match:{$and:[{'Category':{$in:[decodeURIComponent(faqCategory)]}}]}},{$group:{_id:{id:"$_id",Question:"$Question",Answer:"$Answer" }}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
          });
    }); 
}
function getFAQCategories(){
   return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('FAQ');
            collection.distinct("Category",function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
          });
    }); 
}

function getMetaData(dashboardName){
   return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('metadata');
       
       collection.aggregate([{$match:{$and:[{'chatBot':{$in:[dashboardName]}}]}},{$group:{_id:{cubeId:"$cubeId",cubeName:"$cubeName",collectionName:"$collectionName",lastRefreshed:"$lastRefreshed"}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
          });
    }); 
}
function addLuisApp(appName, appId){
      return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('LuisApps');
       var jsonCode = {
           "AppName":appName,
           "AppId":appId,
       };
       collection.insert(jsonCode,function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
          });
    }); 
}

function updateMetaData(mstrCubeId){
   return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('metadata');
       
       collection.update({cubeId: mstrCubeId}, {$set: {lastRefreshed: new Date().toDateString()+' '+new Date().toLocaleTimeString()}},function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
          });
    }); 
}


function getDistinctDROPeriod(){
     return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('dro');
            collection.aggregate([{"$group":{"_id":{period:"$Period",periodid:"$Period ID"}}},{$sort:{"_id.periodid":-1}}]).toArray(function(err, items) {
            //collection.distinct("Period",function(err, items) {
            if (err) {
                reject(err);
            } else {
                //resolve(items.sort((a,b) => a>b).slice(items.length-10,items.length));
                var result = [];
                items.forEach(function(item){
                    //if(item.indexOf('17') > 0){
                        result.push(item._id.period);
                  //  }
                });
                resolve(result);
            }          
            });
          });
    });
}
function getPromptArray(PromptType){
    return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('arwip');
        if(PromptType == 1)
       {   
            collection.distinct("Business DESC",function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
        else if(PromptType == 2)
       {   
            collection.distinct("Region DESC",function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
         else if(PromptType == 3)
       {   
            collection.aggregate([{"$group":{"_id":{period:"$Period DESC MIN",periodid:"$Period ID"}}},{$sort:{"_id.periodid":-1}},{$limit:12}]).toArray(function(err, items) {
            //collection.distinct("Period",function(err, items) {
            if (err) {
                reject(err);
            } else {
                //resolve(items.sort((a,b) => a>b).slice(items.length-10,items.length));
                var result = [];
                items.forEach(function(item){
                    //if(item.indexOf('17') > 0){
                        result.push(item._id.period);
                  //  }
                });
                resolve(result);
            }          
            });
       }
          });
    });
    }
//Get Premium Analytics for single dimension
function getPremiumAnalytics(Dimensions){
    return new Promise(function(resolve, reject) {
        var output = []; 
        var extServerOptions = {
        host: 'localhost',
        port:80,
        headers: {
            'Content-Type': 'application/json'  
        },
        //below path yet to be created as web API, it takes one dimension as argument
        path: '/MicrostrategyMVC/Warehouse/GetPremiumByDimension?Dimension='+Dimensions,
        method: 'GET'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
            res.on('data', function(data) {
            responseString += data;
        });
        
        res.on('end', function() {
            var responseObject = JSON.parse(responseString);
            responseObject.forEach(function(val){
            output.push({'Header':val.Header,
            'Value':parseInt(val.Value)}); 
            });
            resolve(output);
            });
        }).end();
    });
}

function aggregateFinanceData(regionsEntity,busniessEntity,periodEntity,metricName){
     return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('arwip');
        if(regionsEntity !='' && busniessEntity !='' && periodEntity != '')
       {     
            collection.aggregate([{$match:{$and:[{'Region DESC':{$in:[regionsEntity]}},{'Business DESC':{$in:[busniessEntity]}},{'Period DESC MIN':{$in:[periodEntity]}}]}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
    else if(regionsEntity !='' && periodEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Region DESC':{$in:[regionsEntity]}},{'Period DESC MIN':{$in:[periodEntity]}}]}},{$group:{_id:{region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
            else if(busniessEntity !='' && periodEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Business DESC':{$in:[busniessEntity]}},{'Period DESC MIN':{$in:[periodEntity]}}]}},{$group:{_id:{business:"$Business DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
         else if(periodEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Period DESC MIN':{$in:[periodEntity]}}]}},{$group:{_id:{period:"$Period DESC MIN" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
         else if(busniessEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Business DESC':{$in:[busniessEntity]}}]}},{$group:{_id:{period:"$Business DESC'" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
         else if(regionsEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Region DESC':{$in:[regionsEntity]}}]}},{$group:{_id:{period:"$Region DESC" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
      });
    });
}
function aggregateDROData(regionsEntity,periodEntity,metricName){
return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('dro');
        if(regionsEntity !='' && periodEntity != '')
       {     
            collection.aggregate([{$match:{$and:[{'Perspective':{$in:['Relationship Manager']}},{'Region':{$in:[regionsEntity]}},{'Period':{$in:[periodEntity]}}]}},{$group:{_id:{region:"$Region",period:"$Period", perspective:"$Perspective" },ar:{$sum:this.totalAR},wip:{$sum:this.totalWIP},total:{ $sum: { $add:this.addmetric}}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
        else if(regionsEntity !='')
       {     
            collection.aggregate([{$match:{$and:[{'Perspective':{$in:['Relationship Manager']}},{'Region':{$in:[regionsEntity]}}]}},{$group:{_id:{region:"$Region", perspective:"$Perspective" },ar:{$sum:totalAR},wip:{$sum:totalWIP},total:{ $sum:{$add:this.addmetric}}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
    else if(periodEntity!= '')
    {
            collection.aggregate([{$match:{$and:[{'Perspective':{$in:['Relationship Manager']}},{'Period':{$in:[periodEntity]}}]}},{$group:{_id:{period:"$Period", perspective:"$Perspective" },ar:{$sum:totalAR},wip:{$sum:totalWIP},total:{ $sum:{$add:this.addmetric}}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
      });
    });
}
function setDROMetrics(metricName){
     if(metricName == 'DRO'){
        this.totalAR = '$AR DROv';
        this.totalWIP ='$WIP DROv';
        this.addmetric = [this.totalAR,this.totalWIP];
    }
    else if(metricName == 'DRO Rolling 12'){
        this.totalAR = '$AR DRO Rolling 12v';
       this.totalWIP ='$WIP DRO Rolling 12v';
        this.addmetric = [this.totalAR,this.totalWIP];
    }
}
function getDROData(regionsEntity,periodEntity,metricName){
    return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('dro');
        if(regionsEntity !='' && periodEntity != ''){
            collection.aggregate([{$match:{$and:[{'Perspective':{$in:['Relationship Manager']}},{'Period':{$in:[periodEntity]}},{'Region':{$in:[regionsEntity]}}]}},{$group:{_id:{region:"$Region",period:"$Period", perspective:"$Perspective", periodid: "$Period ID"},ar:{$sum:this.totalAR},wip:{$sum:this.totalWIP},total:{ $sum: { $add : this.addmetric}}}},{ $sort : { "_id.periodid": -1 }}]).toArray(function(err, items) {
                   if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
        }
        else if(regionsEntity !='')
       {     
            collection.aggregate([{$match:{$and:[{'Perspective':{$in:['Relationship Manager']}},{'Region':{$in:[regionsEntity]}}]}},{$group:{_id:{region:"$Region",period:"$Period", perspective:"$Perspective", periodid: "$Period ID"},ar:{$sum:this.totalAR},wip:{$sum:this.totalWIP},total:{ $sum: { $add : this.addmetric}}}},{ $sort : { "_id.periodid": -1 }}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
    else if(periodEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Perspective':{$in:['Relationship Manager']}},{'Period':{$in:periodEntity}}]}},{$group:{_id:{region:"$Region",period:"$Period", perspective:"$Perspective", periodid: "$Period ID"},ar:{$sum:this.totalAR},wip:{$sum:this.totalWIP},total:{ $sum: { $add : this.addmetric}}}},{ $sort : { "_id.periodid": -1 }}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
      });
    });
}

function getFinanceData(regionsEntity,busniessEntity,periodEntity,metricName){
     return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('arwip');
        if(regionsEntity !='' && busniessEntity !='' && periodEntity != '')
       {     
            collection.aggregate([{$match:{$and:[{'Region DESC':{$in:[regionsEntity]}},{'Business DESC':{$in:[busniessEntity]}},{'Period DESC MIN':{$in:[periodEntity]}}]}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
    else if(regionsEntity !='' && periodEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Region DESC':{$in:[regionsEntity]}},{'Period DESC MIN':{$in:[periodEntity]}}]}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}},{ $sort : { "_id.business": 1 }}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
            else if(busniessEntity !='' && periodEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Business DESC':{$in:[busniessEntity]}},{'Period DESC MIN':{$in:[periodEntity]}}]}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}},{ $sort : { "_id.region": 1 }}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
         else if(periodEntity!= '')
       {     
           collection.aggregate([{$match:{'Period DESC MIN':{$in:[periodEntity]}}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}},{$sort:{"_id.business":-1}}]).toArray(function(err,items){
           // collection.aggregate([{$match:{Period:{$in:[periodEntity]}}},{$group:{_id:{business:"$Business",region:"$Region",period:"$Period" },v:{$sum:"$ARv"}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
         else if(busniessEntity!= '')
       {     
            collection.aggregate([{$match:{'Business DESC':{$in:[busniessEntity]}}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN",periodid:"$Period ID" },v:{$sum:metricName}}},{ $sort : { "_id.periodid": -1 }},{$limit:48}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
          else if(regionsEntity!= '')
       {     
            collection.aggregate([{$match:{'Region DESC':{$in:[regionsEntity]}}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN",periodid:"$Period ID" },v:{$sum:metricName}}},{ $sort : { "_id.periodid": -1 }},{$limit:60}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
      });
    });
}

function getFinanceDataWithMultiplePrompts(regionsEntity,busniessEntity,periodEntity,metricName){
     return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection('arwip');
        if(regionsEntity !='' && busniessEntity !='' && periodEntity != '')
       {     
            collection.aggregate([{$match:{$and:[{'Region DESC':{$in:regionsEntity}},{'Business DESC':{$in:busniessEntity}},{'Period DESC MIN':{$in:periodEntity}}]}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
    else if(regionsEntity !='' && periodEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Region DESC':{$in:regionsEntity}},{'Period DESC MIN':{$in:periodEntity}}]}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}},{ $sort : { "_id.business": 1 }}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
            else if(busniessEntity !='' && periodEntity!= '')
       {     
            collection.aggregate([{$match:{$and:[{'Business DESC':{$in:busniessEntity}},{'Period DESC MIN':{$in:periodEntity}}]}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}},{ $sort : { "_id.region": 1 }}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
         else if(periodEntity!= '')
       {     
            collection.aggregate([{$match:{'Period DESC MIN':{$in:periodEntity}}},{$group:{_id:{business:"$Business DESC",region:"$Region DESC",period:"$Period DESC MIN" },v:{$sum:metricName}}}]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
       }
      });
    });
}


//Get Claims Analytics for single dimension
function getClaimsAnalytics(Dimensions){
    return new Promise(function(resolve, reject) {
        var output = []; 
        var extServerOptions = {
        host: 'localhost',
        port:80,
        headers: {
            'Content-Type': 'application/json'
        },
        //below path yet to be created as web API, it takes one dimension as argument
        path: '/MicrostrategyMVC/Warehouse/GetClaimsByDimension?Dimension='+Dimensions,
        method: 'GET'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
            res.on('data', function(data) {
            responseString += data;
        });
        res.on('end', function() {
            var responseObject = JSON.parse(responseString);
            responseObject.forEach(function(val){
            output.push({'Header':val.Header,
            'Value':parseInt(val.Value)}); 
            //reports.push(val.AggregatedClaims); //'AggregatedClaims'' will be replaced by metric name returned by query
            });
            resolve(output);
            });
        }).end();
    });
}
function getReportListByMetric(MetricName){
return new Promise(function(resolve, reject) {
        var reports = [];
        var extServerOptions = {
        host: 'localhost',
        port:80,
        headers: {
            'Content-Type': 'application/json'
        },
        path: '/MicrostrategyMVC/metadata/GetReportListByMetric?CountType='+MetricName,
        method: 'GET'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
            res.on('data', function(data) {
            responseString += data;
        });
        res.on('end', function() {
            var responseObject = JSON.parse(responseString);
            responseObject.forEach(function(val){
            //reports.push(val.IS_DOC_NAME);
            reports.push({'ReportName':val.IS_DOC_NAME,
            'ReportGUID':val.IS_DOC_ID});
            });
            resolve(reports);
            });
        }).end();
    });
}
function getCubeCount(countType){
   return new Promise(function(resolve, reject) {
        var output = []; 
        var extServerOptions = {
        host: 'localhost',
        port:80,
        headers: {
            'Content-Type': 'application/json'
        },
        //below path yet to be created as web API, it takes one dimension as argument
        path: '/MicrostrategyMVC/metadata/GetCubeCount?CountType='+countType,
        method: 'GET'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
            res.on('data', function(data) {
            responseString += data;
        });
        res.on('end', function() {
            var responseObject = JSON.parse(responseString);
            output.push({'Header':responseObject.Head,
            'Value':parseInt(responseObject.Count)}); 
            resolve(output);
            });
        }).end();
    }); 
}
function getUnmappedUtterances(){
   return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {

        var collection = db.collection('Utterance');
            collection.aggregate([{ $unwind: "$query" },{$group: {_id: {$toLower: '$query'},count: { $sum: 1 }}},{$match: {            count: { $gte: 0 }}},{ $sort : { count : -1} },{ $limit : 10 }]).toArray(function(err, items) {
            if (err) {
                reject(err);
            } else {
                resolve(items);
            }          
            });
          });
    }); 
}
function getReportCount(countType){
    return new Promise(function(resolve, reject) {
        var output = []; 
        var extServerOptions = {
        host: 'localhost',
        port:80,
        headers: {
            'Content-Type': 'application/json'
        },
        //below path yet to be created as web API, it takes one dimension as argument
        path: '/MicrostrategyMVC/metadata/GetReportCount?CountType='+countType,
        method: 'GET'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
            res.on('data', function(data) {
            responseString += data;
        });
        res.on('end', function() {
            var responseObject = JSON.parse(responseString);
            output.push({'Header':responseObject.Head,
            'Value':parseInt(responseObject.Count)}); 
            resolve(output);
            });
        }).end();
    });
}
function deleteCubeData(collectionName){
     return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection(collectionName);
          collection.deleteMany({}, function(err, obj) {
                if (err)
                    console.log(err);
                else{
                //console.log(obj.result.n + " document(s) deleted");
                }
            });
        resolve('done');
      });
    });
}
function insertCubeData(jsonData,collectionName){
     return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
         resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection(collectionName);
             collection.insertMany(jsonData, function(err, res) {
                if (err) 
                    console.log(err);
                else{
                //console.log("Number of documents inserted: " + res.insertedCount);
                }
            });
        resolve('done');
      });
    });
}
function saveUtterance(data){
     return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
          resolve(db.db(dbName));//resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection("Utterance");
             collection.insert(data, function(err, res) {
                if (err) 
                    console.log(err);
                else{
                }
            });
        resolve('done');
      });
    });
}
function saveLuisResponse(query){
     return new Promise(function(resolve, reject) {
      MongoClient.connect(dbPath, function(err, db) {
        if (err) {
          reject(err);  
        } else {
          resolve(db.db(dbName));//resolve(db.db(dbName));
        }        
      })
    }).then(function(db) {
      return new Promise(function(resolve, reject) {
        var collection = db.collection("Luis");
             collection.insert(data, function(err, res) {
                if (err) 
                    console.log(err);
                else{
                }
            });
        resolve('done');
      });
    });
}
function getIntent(utterance){
 return new Promise(function(resolve, reject) {
    var luisData;
      var endpoint = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/";
     var luisAppId = '';
    
    var queryParams = {
          "subscription-key": "",
        "timezoneOffset": "5.5",
        "verbose":  true,
        "q": utterance
    }
    
var proxiedRequest = request.defaults({'proxy': process.env.PROXY_URL});
    var luisRequest =
        endpoint + luisAppId +
        '?' + querystring.stringify(queryParams);

   proxiedRequest(luisRequest,
        function (err,
            response, body) {
            if (err){
                
            resolve(err);
            }
            else {
                luisData = JSON.parse(body);
                 //saveUtterance(luisData);
                 resolve(luisData);
            }
        })
    });
}
function getFAQIntent(utterance){
 return new Promise(function(resolve, reject) {
    var luisData;
      var endpoint = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/";
     var luisAppId = '';
    
    var queryParams = {
          "subscription-key": "",
        "timezoneOffset": "5.5",
        "verbose":  true,
        "q": utterance
    }
    
var proxiedRequest = request.defaults({'proxy': process.env.PROXY_URL});
    var luisRequest =
        endpoint + luisAppId +
        '?' + querystring.stringify(queryParams);

   proxiedRequest(luisRequest,
        function (err,
            response, body) {
            if (err){
                
            resolve(err);
            }
            else {
                luisData = JSON.parse(body);
                 //saveUtterance(luisData);
                 resolve(luisData);
            }
        })
    });
}


module.exports = {
    deleteCubeData :function(collectionName){
    return new Promise(function (resolve) {
    var result = deleteCubeData(collectionName);
             setTimeout(function () { resolve(result); }, 1000);
         });
    },
        insertCubeData :function(jsonData,collectionName){
    return new Promise(function (resolve) {
    var result = insertCubeData(jsonData,collectionName);
             setTimeout(function () { resolve(result); }, 1000);
         });
    },
    getIntent:function(utterance){
    return new Promise(function (resolve) {
    var result = getIntent(utterance);
             setTimeout(function () { resolve(result); }, 1000);
         });
    },
    getFAQIntent:function(utterance){
    return new Promise(function (resolve) {
    var result = getFAQIntent(utterance);
             setTimeout(function () { resolve(result); }, 1000);
         });
    },
    saveLuisResponse:function(response){
    return new Promise(function (resolve) {
    var result = saveLuisResponse(response);
             setTimeout(function () { resolve(result); }, 1000);
         });
    },
    getUnmappedUtterances:function(){
    return new Promise(function (resolve) {
    var result = getUnmappedUtterances();
             setTimeout(function () { resolve(result); }, 1000);
         });
    },
    saveUtterance:function(response){
    return new Promise(function (resolve) {
    var result = saveUtterance(response);
             setTimeout(function () { resolve(result); }, 1000);
         });
    },
    searchProjects:function(){
    return new Promise(function (resolve) {
    var projectlist = getProjectList();
             setTimeout(function () { resolve(projectlist); }, 1000);
         });
    },
    searchReports:function(projectName){
    return new Promise(function (resolve) {
    var reportlist = getReportList(projectName);
             setTimeout(function () { resolve(reportlist); }, 1000);
         });
    },
    PremiumResponse:function(Dimensions){
    return new Promise(function (resolve) {
    var PremiumOutput = getPremiumAnalytics(Dimensions);
             setTimeout(function () { resolve(PremiumOutput); }, 1000);
         });
    },
    FAQListResponse: function(faqCategory){
        return new Promise(function (resolve){
            var FQAList = getFAQList(faqCategory);
             setTimeout(function () { resolve(FQAList); }, 1000);
        });
    },
    FAQCategoryResponse: function(){
        return new Promise(function (resolve){
            var FAQCategories = getFAQCategories();
             setTimeout(function () { resolve(FAQCategories); }, 1000);
        });
    },
    DROPeriodResponse:function(){
    return new Promise(function (resolve) {
    var PromptOutput = getDistinctDROPeriod();
             setTimeout(function () { resolve(PromptOutput); }, 1000);
         });
    },
    GetMetaDataResponse:function(DashboardName){
    return new Promise(function (resolve) {
    var PromptOutput = getMetaData(DashboardName);
             setTimeout(function () { resolve(PromptOutput); }, 1000);
         });
    },    
    UpdateMetaDataResponse:function(cubeId){
    return new Promise(function (resolve) {
    var PromptOutput = updateMetaData(cubeId);
             setTimeout(function () { resolve(PromptOutput); }, 1000);
         });
    }, 
    addLuisAppResponse :function(appName, appId){
    return new Promise(function (resolve) {
    var PromptOutput = addLuisApp(appName, appId);
             setTimeout(function () { resolve(PromptOutput); }, 1000);
         });
    },      
    PromptResponse:function(PromptType){
    return new Promise(function (resolve) {
    var PromptOutput = getPromptArray(PromptType);
             setTimeout(function () { resolve(PromptOutput); }, 1000);
         });
    },
    ClaimsResponse:function(Dimensions){
    return new Promise(function (resolve) {
    var ClaimsOutput = getClaimsAnalytics(Dimensions);
             setTimeout(function () { resolve(ClaimsOutput); }, 1000);
         });
    },
    FinanceAggregateResponse:function(regionsEntity,busniessEntity,periodEntity,metricName){
    return new Promise(function (resolve) {
    var FinanceOutput = aggregateFinanceData(regionsEntity,busniessEntity,periodEntity,metricName);
             setTimeout(function () { resolve(FinanceOutput); }, 1000);
         });
    },
    FinanceResponse:function(regionsEntity,busniessEntity,periodEntity,metricName){
    return new Promise(function (resolve) {
    var FinanceOutput = getFinanceData(regionsEntity,busniessEntity,periodEntity,metricName);
             setTimeout(function () { resolve(FinanceOutput); }, 1000);
         });
    },
    DROResponse:function(regionsEntity,periodEntity,metricName){
    return new Promise(function (resolve) {
    setDROMetrics(metricName);
    var FinanceOutput = getDROData(regionsEntity,periodEntity,metricName);
             setTimeout(function () { resolve(FinanceOutput); }, 1000);
         });
    },
    DROAggregateResponse:function(regionsEntity,periodEntity,metricName){
    return new Promise(function (resolve) {
    setDROMetrics(metricName);
    var FinanceOutput = aggregateDROData(regionsEntity,periodEntity,metricName);
             setTimeout(function () { resolve(FinanceOutput); }, 1000);
         });
    },
    FinanceResponseWithMultiplePrompts:function(regionsEntity,busniessEntity,periodEntity,metricName){
    return new Promise(function (resolve) {
    var FinanceOutput = getFinanceDataWithMultiplePrompts(regionsEntity,busniessEntity,periodEntity,metricName);
             setTimeout(function () { resolve(FinanceOutput); }, 1000);
         });
    },
    ReportCountResponse:function(countType){
    return new Promise(function (resolve) {
    var reportCount = getReportCount(countType);
             setTimeout(function () { resolve(reportCount); }, 1000);
         });
    },
        CubeCountResponse:function(countType){
    return new Promise(function (resolve) {
    var cubeCount = getCubeCount(countType);
             setTimeout(function () { resolve(cubeCount); }, 1000);
         });
    },
        ReportListByMetricResponse:function(countType){
    return new Promise(function (resolve) {
    var reportList = getReportListByMetric(countType);
             setTimeout(function () { resolve(reportList); }, 1000);
         });
    },
    SqlDataResponse:function(sqlQuery){
    return new Promise(function (resolve) {
    var dataset = getSqlData(sqlQuery);
             setTimeout(function () { resolve(dataset); }, 1000);
         });
    }
};