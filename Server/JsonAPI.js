var Promise = require('bluebird');

var request = require('request-promise');
var querystring = require('querystring');
var http = require('http');
var decode = require('decode-html');
// ******** Configuration

    //REST Server
    var RESTServerIP = "10.52.12.11";                 // Logical name of the REST Server
    var RESTServerPort = "10086";                 // Port of the REST Server
    var ContentType = "application/vnd.mstr.dataapi.v0+json"; // To be added in each request as a request header

    //MicroStrategy Intelligence Server
    var IServer = "10.52.12.11";                  // Logical name of the MicroStrategy IServer
    var IServerPort = 34952;                      // Port of the MicroStrategy IServer
    var projectName = "RM";   // Name of the MicroStrategy Project
    var username = "administrator";               // Credentials to connect to the MicroStrategy Project
    var password = "madmin";

    //MicroStrategy Object
    var isCube = true;                            // True if running a cube, false if running a report
    var id = "CB12C2844E81CE71ECBB8D8193468BE2";  // Id of the cube or report used
    var itemPerPage = 10000;                         // Number of items the table will display per page


    // ******** Constants

    var BASE_URL = "http://" + RESTServerIP + ":" + RESTServerPort + "/json-data-api/";
    var TARGET = isCube ? "cubes/" : "reports/";
    var CURRENT_URL = BASE_URL + TARGET;
    var UPDATE_PAGINATION_MODE = {
        INIT: 0,
        NEXT: 1,
        PREV: 2
    };
    var LIST_VIEW = {
        attribute: 0,
        metric: 1
    };

    // ******** Global variables

    var json;                                      // Contains the retrieved json output
    var authToken;                                 // Contains the session id which will be used in every api call
    var instanceId;                                // Contains the instance id which is a reference to a report created in memory.
    var paging = {                                 // Used to handle pagination
        offset: 0,
        limit: itemPerPage
    };
    var autoload = true;                           // Determine if the api call should be made as soon as we select at least one attribute and one metric or using a apply button
    var attributeList = null;                      // Contains the attribute list of the json output
    var metricList = null;                         // Contains the metric list of the json output
    var attributeListView, metricListView;         // Declare list views
    var postBody;                                  // Contains the input body of the api call



    //  var BASE_URL = "http://" + RESTServerIP + ":" + RESTServerPort + "/json-data-api/";
    // var TARGET = isCube ? "cubes/" : "reports/";
    // var CURRENT_URL = BASE_URL + TARGET;
    //http://10.52.12.11:10086/json-data-api/cubes/1033A63E4654EEAD713598A78D347514/instances?offset=0&limit=10000

    //  $.ajax({
    //         type: 'POST',
    //         url: BASE_URL + "sessions",
    //         beforeSend: function (xhr) {
    //             xhr.setRequestHeader("X-IServerName", IServer);
    //             xhr.setRequestHeader("X-Port", IServerPort);
    //             xhr.setRequestHeader("X-ProjectName", projectName);
    //             xhr.setRequestHeader("X-Username", username);
    //             xhr.setRequestHeader("Accept", ContentType);
    //             xhr.setRequestHeader("X-Password", password);
    //         },
    //         success: function (response) {
    //             authToken = response.authToken;
    //             getData();
    //         },
    //         error: function (e) {
    //             console.log(e);
    //         }
    //     });
    function getSession(){
    return new Promise(function(resolve, reject) {
        var extServerOptions = {
        host: RESTServerIP,
        port:10086,
        headers: {
            'Content-Type': 'application/json',
            'X-IServerName': IServer,
            'X-Port': IServerPort,
            'X-ProjectName': projectName,
            'X-Username': username,
            'Accept': ContentType,
            'X-Password': password
        },
        path:  "/json-data-api/sessions/",
        method: 'POST'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
        res.on('data', function(data) {
        responseString += data;
        });
        res.on('end', function() {
        var responseObject = JSON.parse(responseString);
        //collection = responseObject;
        //responseObject.forEach(function(val){
        //collection.push(responseObject.authToken);
        //});
        resolve(responseObject);
        });
    }).end();
  });
}
 var attributes = [];
            var metrics = [];
            var data = [];
            var row = [];

function getCubeData(authToken,cubeId){
    attributes = [];
    metrics = [];
      data = [];
             row = [];
    return new Promise(function(resolve, reject) {
        var collection = [];
        var extServerOptions = {
        host: RESTServerIP,
        port:10086,
        headers: {
            //'Content-Type': 'application/json',
            'X-MSTR-AuthToken' : authToken,
            'Accept':ContentType
        },
        path:  "/json-data-api/cubes/" + cubeId + "/instances?offset=" + 0 + "&limit=" + paging.limit,
        method: 'POST'
        };
        var responseString='';
        http.request(extServerOptions, function (res) {
        res.on('data', function(data) {
        responseString += data;
        });
        res.on('end', function() {
         var responseObject = JSON.parse(responseString);
          var jsonData = [];

                var definition = responseObject.result.definition;

                //Add attributes in the columns of the table
                definition.attributes.forEach(function (attribute) {
                    attributes.push(attribute.name);
                });

                //Add metrics in the columns of the table
                definition.metrics.forEach(function (metric) {
                    metrics.push(metric.name);
                });


           search(responseObject.result.data.root);
            data.forEach(function(element) {
                     var item = {};
                     var count = 0;
                       definition.attributes.forEach(function (attribute) {
                    item[attribute.name]= element[count++];
                });
                   definition.metrics.forEach(function (metric) {
                       if(metric.name.indexOf('Row Count')== -1){
                    item[metric.name+'v']= element[count].v;
                    item[metric.name+'f']= element[count++].f;
                       }else{
                           count++;
                       }
                });
                    jsonData.push(item);
                }, this);
        resolve(jsonData);
        });
    }).end();
  });
}
function visit(node) {
                /*  Apply what needs to be done when visiting a node
                 *  @param {Object} node visited */

                if (node.element){
                    row[node.depth] = decode(node.element.name);
                }
                if (node.metrics) {
                    metrics.forEach(function (metric, i) {
                        row[node.depth + 1 + i] = {
                            v: (node.metrics[metric].rv),
                            f: (node.metrics[metric].fv)
                        };
                    });
                    data.push(row);
                    row = row.slice();
                }
            }

            function search(root) {
                /*  Goes recursively though the input root tree
                 *  @param {Object} root node of the tree to be searched */

                if (root) {
                    visit(root);
                    root.visited = true;
                    if (!root.metrics) {
                        root.children.forEach(function (node) {
                            if (!node.visited) {
                                search(node);
                            }
                        });
                    }
                }
            }
module.exports = {
      getSession:function(){
    return new Promise(function (resolve) {
    var result = getSession();
             setTimeout(function () { resolve(result); }, 1000);
         });
    },
     getCubeData:function(token,id){
    return new Promise(function (resolve) {
    var result = getCubeData(token,id);
             setTimeout(function () { resolve(result); }, 1000);
         });
    }
    };